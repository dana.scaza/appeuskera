module Main where

import BasicData
import Data.Default
import Database.Bolt
import Exercises.Generation
import Features
import Neo4J
import Syntax.ProtoSentence
import System.Random

itemNum :: Int
itemNum = 8

main :: IO ()
main = do
  g <- getStdGen
  let initialState = (g, protoCopula)
  pipe <- connect myConfiguration
  menu initialState pipe
  close pipe
  putStrLn "pipe closed. bye!"

menu :: SentenceState -> Pipe -> IO SentenceState
menu state pipe = do
  putStrLn
    ( "Elige un ejercicio:\n"
        ++ "1  -  Izan: presente \n"
        ++ "2  -  Izan: pasado simple\n"
        ++ "3  -  Nor-nork: presente perfecto\n"
        ++ "4  -  Nor-nork: futuro\n"
        ++ "5  -  Posesivos\n"
        ++ "0  -  Salir\n"
    )
  answer <- getLine
  processChoice answer state pipe

processChoice :: String -> SentenceState -> Pipe -> IO SentenceState
processChoice choice state pipe
  | choice == "1" = do
    newState <- copulaItemsPresent itemNum pipe state
    menu newState pipe
  | choice == "2" = do
    newState <- copulaItemsPast itemNum pipe state
    menu newState pipe
  | choice == "3" = do
    newState <- nor3NorkItems itemNum pipe Present Perfective state
    menu newState pipe
  | choice == "4" = do
    newState <- nor3NorkItems itemNum pipe Present Future state
    menu newState pipe
  | choice == "5" = do
    newState <- possessiveItems itemNum pipe state
    menu newState pipe
  | choice == "0" = return state
  | otherwise = do
    putStrLn "Por favor, elige una de las opciones disponibles."
    menu state pipe

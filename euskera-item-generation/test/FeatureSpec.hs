module FeatureSpec where

import Features
import Test.Hspec
import Test.QuickCheck

spec :: Spec
spec = do
  describe "Number nominal feature monoid instance" $ do
    it "respects the IDENTITY laws" $ do
      property $ \x ->
        x <> mempty == x && mempty <> x == (x :: Number)
    it "respects the Associativity law" $ do
      property $ \x y z ->
        (x <> y) <> (z :: Number) == x <> (y <> z)
  describe "Person nominal feature monoid instance" $ do
    it "respects the IDENTITY laws" $ do
      property $ \x ->
        x <> mempty == x && mempty <> x == (x :: Person)
    it "respects the Associativity law" $ do
      property $ \x y z ->
        (x <> y) <> (z :: Person) == x <> (y <> z)

instance Arbitrary Number where
  arbitrary = elements [Singular, Plural, Zero]

instance Arbitrary Person where
  arbitrary = elements [First, Second, Third]

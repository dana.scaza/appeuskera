-- |
-- Module      : BasicData
--
-- This module contains data such as features, lexis and constituents
-- needed to initialize
-- other structures.
module BasicData where

import Features
import Lexis
import Syntax.Constituent

--------------------------------------------------------------------------------
-- Features
--------------------------------------------------------------------------------

-- | Starter features for nor constituents
norArgF = NF Third Singular Absolutive NoPoss Definite False

-- | Starter features for nouns
baseNounF = norArgF

-- | Starter features for nori arguments
noriArgF = NF Third Singular Dative NoPoss Definite False

-- | Starter features for nork arguments
norkArgF = NF Second Singular Ergative NoPoss Definite False

-- | Starter features for verb phrases
baseVF = VF Present Punctual norArgF noriArgF norkArgF Full

-- --------------------------------------------------------------------------------
-- -- Constituents
-- --------------------------------------------------------------------------------

-- | The adverb "ez" ('no')
ez = L ["no"] $ Adv (A "ez") BareAdv

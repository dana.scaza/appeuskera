module Syntax.SentenceModels (createSentence) where

import BasicData
import EuskeraAppError
import Features
import Lexis
import Syntax.Constituent
import Syntax.NominalPhrases
import Syntax.ProtoSentence
import Syntax.VerbalPhrases

-- | Given a 'ProtoSentence', generate a Sentence constituent
createSentence :: ProtoSentence -> Sentence
createSentence s = case mainVerb s of
  Nor {} -> error "Unimplemented"
  Copula {} -> copula s
  NorNork {} -> norNork s
  NorNori {} -> error "Unimplemented"
  NorNoriNork {} -> error "Unimplemented"

copula :: ProtoSentence -> Sentence
copula s =
  case (sentType s, negated s) of
    (Declarative, True) -> addConstituents s [norCons s, no, aux, attr, v]
    (Declarative, False) -> addConstituents s [norCons s, attr, v]
    (YesNoInterrogative, True) -> addConstituents s [norCons s, no, aux, attr, v]
    (YesNoInterrogative, False) -> addConstituents s [norCons s, attr, v]
  where
    attr = agreeNPs (norCons s) (attribute s)
    aux = agree (auxiliary (Nor "izan") vf)
    v = agree . L ["mainVerb"] $ V (Copula "izan") (mainVerbFeatures vf (negated s))
    vf = genVF s
    agree = norAgreement (norCons s)

norNork :: ProtoSentence -> Sentence
norNork s =
  case (sentType s, negated s) of
    (Declarative, False) -> addConstituents s [norkCons s, norCons s, v]
    (Declarative, True) -> addConstituents s [norkCons s, no, aux, norCons s, v]
    (YesNoInterrogative, False) -> addConstituents s [norkCons s, norCons s, v]
    (YesNoInterrogative, True) -> addConstituents s [norkCons s, no, aux, norCons s, v]
  where
    aux = agree (auxiliary (NorNork "ukan") vf)
    v = agree . L ["mainVerb"] $ V (mainVerb s) (mainVerbFeatures vf (negated s))
    vf = genVF s
    agree = norNorkAgreement (norkCons s) (norCons s)

no :: Constituent
no = addTag "mainVerb" ez

addConstituents :: ProtoSentence -> [Constituent] -> Sentence
addConstituents s = S [] (SF $ sentType s)

auxiliary :: Verb -> VF -> VPConstituent
auxiliary v vf = L ["mainVerb", "aux"] $ V v (auxOrTrinko vf)

genVF :: ProtoSentence -> VF
genVF s = baseVF {tense = tenseS s, aspect = aspectS s}

mainVerbFeatures :: VF -> Bool -> VF
mainVerbFeatures vf True = lexicalVerb vf
mainVerbFeatures vf False = fullVerb vf

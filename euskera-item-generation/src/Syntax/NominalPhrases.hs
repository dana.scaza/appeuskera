module Syntax.NominalPhrases
  ( np,
    coordNP,
    agreeNPs,
    nounToNPWithDetAndNum,
    nounToNPWithNumber,
  )
where

import Features
import Lexis
import Syntax.Constituent

-- | Create a Noun Phrase with given Lexis and nominal features
np :: Lexis -> NF -> NPConstituent
np l nf = L [] (modifyFeatures l (WrapNF nf))

-- | Generate a coordinated Noun Phrase taking into account its members features
coordNP :: NPConstituent -> NPConstituent -> NPConstituent
coordNP np1 np2 =
  NP [] nf1 {person = p, number = Plural, coord = True} [np1, np2]
  where
    (WrapNF nf1) = getFeatures np1
    (WrapNF nf2) = getFeatures np2
    p = person nf1 <> person nf2

-- | This function creates an NP from a Number feature and a Lexis
--  Mass nouns are always Singular, and plural nouns Plural
--  Moreover, Nouns are always third person
nounToNPWithNumber :: Number -> Lexis -> NPConstituent
nounToNPWithNumber = nounToNPWithDetAndNum Definite

-- | This function creates an NP from a Number feature, a Determiner features and a Lexis
--  Mass nouns are always Singular, and plural nouns Plural;
--  nouns are always third person,
--  and mass and plural nouns do not take indefinite determination
nounToNPWithDetAndNum :: Determiner -> Number -> Lexis -> NPConstituent
nounToNPWithDetAndNum det num (N (Noun (Count noun defNum)) nf) =
  case defNum of
    Singular ->
      L [] $ N (Noun (Count noun defNum)) (nf {number = num, person = Third, determiner = det})
    Plural ->
      L [] $
        N
          (Noun (Count noun defNum))
          ( nf
              { number = Plural,
                person = Third,
                determiner = chooseDeterminerForNonSingNoun (nf {determiner = det})
              }
          )
nounToNPWithDetAndNum det num (N (Noun (Mass noun)) nf) =
  L [] $
    N
      (Noun (Mass noun))
      ( nf
          { number = Singular,
            person = Third,
            determiner = Definite
          }
      )

-- make sure that mass and plural nouns do not have indefinite determination
chooseDeterminerForNonSingNoun :: NF -> Determiner
chooseDeterminerForNonSingNoun nf =
  case determiner nf of
    Indefinite -> Definite
    _ -> determiner nf

-- | Given two constituents, make the second one agree with the first.
-- Used mainly in copula sentences.
agreeNPs :: NPConstituent -> NPConstituent -> NPConstituent
agreeNPs (NP _ srcF _) (NP tgs tgtF cs) =
  NP tgs (copyNF srcF tgtF) cs
agreeNPs (L _ (N _ srcF)) (L tgs (N nom tgtF)) = L tgs (N nom $ copyNF srcF tgtF)
agreeNPs (L _ (N _ srcF)) (NP tgs tgtF cs) =
  NP tgs (copyNF srcF tgtF) cs
agreeNPs (NP _ srcF _) (L tgs (N nom tgtF)) = L tgs (N nom $ copyNF srcF tgtF)
agreeNPs (L _ (N _ srcF)) (L tgs (Adj adj tgtF)) = L tgs (Adj adj $ copyNF srcF tgtF)
agreeNPs (NP _ srcF _) (L tgs (Adj adj tgtF)) = L tgs (Adj adj $ copyNF srcF tgtF)
agreeNPs src tgt = error ("error items: " ++ show src ++ show tgt)

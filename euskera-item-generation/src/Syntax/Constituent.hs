module Syntax.Constituent
  ( NPConstituent,
    VPConstituent,
    Sentence,
    Tag,
    Constituent (..),
    getFeatures,
    getTags,
    addTag,
    addTags,
    getConstituents,
    setConstituents,
    instantiate,
  )
where

import Data.Char as Char
import Data.List
import EuskeraAppError
import Features
import Lexis
import Morphology.Inflection

type NPConstituent = Constituent

type VPConstituent = Constituent

type Sentence = Constituent

type Tag = String

-- | Syntactic tree, tagged
data Constituent
  = -- | Sentential constituent
    S [Tag] SF [Constituent]
  | -- | Verbal phrase
    VP [Tag] VF [Constituent]
  | -- | Noun phrase
    NP [Tag] NF [Constituent]
  | -- | Bare lexical item
    L [Tag] Lexis
  | -- | Gap in substitution of constituent. If set to True, the gap will be
    -- a horizontal line. Otherwise, it will be an invisible gap.
    FillGap Bool
  deriving (Show)

--------------------------------------------------------------------------------

getFeatures :: Constituent -> FeatureWrapper
getFeatures (S _ f _) = wrap f
getFeatures (VP _ f _) = wrap f
getFeatures (NP _ f _) = wrap f
getFeatures (L _ lexis) = getLFeatures lexis
getFeatures _ = error "Constituent type without features"

getTags :: Constituent -> [Tag]
getTags (S ts _ _) = ts
getTags (VP ts _ _) = ts
getTags (NP ts _ _) = ts
getTags (L ts _) = ts
getTags FillGap {} = []

-- | Add one tag as the head of the Constituent's tag list
addTag :: Tag -> Constituent -> Constituent
addTag tag (S ts sf cs) = S (tag : ts) sf cs
addTag tag (VP ts vf cs) = VP (tag : ts) vf cs
addTag tag (NP ts nf cs) = NP (tag : ts) nf cs
addTag tag (L ts l) = L (tag : ts) l

-- | Add a list of tags as the head of the Constituent's tag list
addTags :: [Tag] -> Constituent -> Constituent
addTags tags (S ts sf cs) = S (tags ++ ts) sf cs
addTags tags (VP ts vf cs) = VP (tags ++ ts) vf cs
addTags tags (NP ts nf cs) = NP (tags ++ ts) nf cs
addTags tags (L ts l) = L (tags ++ ts) l

-- | Create new constituent by adding the constituent list the the source constituent
-- For constructors with no constituent list, the constituent is returned unchanged
setConstituents :: Constituent -> [Constituent] -> Constituent
setConstituents (S ts sf _) cs = S ts sf cs
setConstituents (VP ts vf _) cs = VP ts vf cs
setConstituents (NP ts nf _) cs = NP ts nf cs
setConstituents x _ = x

-- | Retrieve Constituents that form fiven Constituent
getConstituents :: Constituent -> [Constituent]
getConstituents (S _ _ cs) = cs
getConstituents (VP _ _ cs) = cs
getConstituents (NP _ _ cs) = cs
getConstituents (L ts l) = [L ts l]
getConstituents _ = []

--------------------------------------------------------------------------------
-- Sentence instantiation
--------------------------------------------------------------------------------

-- | Transform a syntactic tree (Constituent) into a sentence (String)
instantiate :: Constituent -> Either EuskeraAppError String
instantiate (FillGap True) = Right "_______" -- depends on features...
instantiate (FillGap False) = Right ""
instantiate (S _ sf xps) = styleSentence sf <$> instantiate' xps
instantiate (NP _ nf nps)
  | coord nf = unwordsAnd <$> sequenceA (instantiate <$> nps)
  | otherwise = instantiate' nps
instantiate (VP _ _ vps) = instantiate' vps
instantiate (L _ word) = inflect word

instantiate' :: [Constituent] -> Either EuskeraAppError String
instantiate' xps = unwords <$> sequenceA (instantiate <$> xps)

-- Join constituents with "eta" (and)
unwordsAnd :: [String] -> String
unwordsAnd [] = ""
unwordsAnd ws = foldr1 (\w s -> w ++ " eta " ++ s) ws

-- Add appropriate punctuation marks
styleSentence :: SF -> String -> String
styleSentence (SF t) (first : rest) = case t of
  Declarative -> s ++ "."
  YesNoInterrogative -> s ++ "?"
  where
    s = dropWhileEnd Char.isSpace (Char.toUpper first : rest)

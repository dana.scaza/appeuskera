module Syntax.ProtoSentence
  ( ProtoSentence (..),
    protoCopula,
    protoNorNork,
    negateSent,
    modifyType,
    modifyAspect,
    modifyTense,
    modifyAttribute,
    modifyNor,
    modifyNork,
    modifyVerb,
  )
where

import BasicData
import Features
import Lexis
import Syntax.Constituent

-- | This type contains all the information required to generate a
-- sentence 'Constituent'. Prefer using default instantiations
-- as starting points of the generations process, instead of
-- generating new ProtoSentences
data ProtoSentence = ProtoSentence
  { -- | Nor Constituent
    norCons :: Constituent,
    -- | Attribute
    attribute :: Constituent,
    -- | Nori Constituent
    noriCons :: Constituent,
    -- | Nork Constituent
    norkCons :: Constituent,
    mainVerb :: Verb,
    sentType :: SentenceType,
    negated :: Bool,
    tenseS :: Tense,
    aspectS :: Aspect
  }
  deriving (Show)

-- | The initial information required to build a copula item
protoCopula =
  ProtoSentence
    { norCons = L ["nor"] (N Pronoun norArgF),
      attribute = L ["attribute"] (N Pronoun norArgF),
      noriCons = L ["nori"] (N Pronoun noriArgF),
      norkCons = L ["nork"] (N Pronoun norkArgF),
      mainVerb = Copula "izan",
      sentType = Declarative,
      negated = False,
      tenseS = Present,
      aspectS = Punctual
    }

-- | The initial information required to build a nor-nork item
protoNorNork =
  ProtoSentence
    { norCons = L ["nor"] (N Pronoun norArgF),
      attribute = L ["attribute"] (N Pronoun norArgF),
      noriCons = L ["nori"] (N Pronoun noriArgF),
      norkCons = L ["nork"] (N Pronoun norkArgF),
      mainVerb = NorNork "ukan",
      sentType = Declarative,
      negated = False,
      tenseS = Present,
      aspectS = Punctual
    }

-- | Generate new ProtoSentence with given SentenceType
modifyType :: SentenceType -> ProtoSentence -> ProtoSentence
modifyType st proto = proto {sentType = st}

-- | Generate new ProtoSentence with negated = True
negateSent :: ProtoSentence -> ProtoSentence
negateSent proto = proto {negated = True}

-- | Generate new ProtoSentence with given Aspect
modifyAspect :: Aspect -> ProtoSentence -> ProtoSentence
modifyAspect asp proto = proto {aspectS = asp}

-- | Generate new ProtoSentence with given Tense
modifyTense :: Tense -> ProtoSentence -> ProtoSentence
modifyTense tns proto = proto {tenseS = tns}

-- | Generate new ProtoSentence with given NPConstituent as nor argument
modifyNor :: NPConstituent -> ProtoSentence -> ProtoSentence
modifyNor np proto = proto {norCons = np}

-- | Generate new ProtoSentence with given NPConstituent as nork argument
modifyNork :: NPConstituent -> ProtoSentence -> ProtoSentence
modifyNork np proto = proto {norkCons = np}

-- | Generate new ProtoSentence with given NPConstituent as attribute
modifyAttribute :: NPConstituent -> ProtoSentence -> ProtoSentence
modifyAttribute np proto = proto {attribute = np}

-- | Generate new ProtoSentence with given 'Verb' as verb
modifyVerb :: Verb -> ProtoSentence -> ProtoSentence
modifyVerb v proto = proto {mainVerb = v}

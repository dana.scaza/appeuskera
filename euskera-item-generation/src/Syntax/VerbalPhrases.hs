module Syntax.VerbalPhrases (norAgreement, norNorkAgreement) where

import Features
import Lexis
import Syntax.Constituent

-- | Update verbal constituent to agree with given nor constituent.
norAgreement :: NPConstituent -> VPConstituent -> VPConstituent
norAgreement (NP _ norF _) (VP t vf cs) =
  VP t (norConcord norF vf) cs
norAgreement (L _ (N _ norF)) (VP t vf cs) =
  VP t (norConcord norF vf) cs
norAgreement (NP _ norF _) (L t (V v vf)) =
  L t (V v (norConcord norF vf))
norAgreement (L _ (N _ norF)) (L t (V v vf)) =
  L t (V v (norConcord norF vf))

-- | Update verbal constituent to agree with given nork (first)
-- and nor (second) constituent.
norNorkAgreement ::
  NPConstituent ->
  NPConstituent ->
  VPConstituent ->
  VPConstituent
norNorkAgreement (NP _ norkF _) norCons (VP t vf cs) =
  norAgreement norCons (VP t (norkConcord norkF vf) cs)
norNorkAgreement (L _ (N _ norkF)) norCons (VP t vf cs) =
  norAgreement norCons (VP t (norkConcord norkF vf) cs)
norNorkAgreement (NP _ norkF _) norCons (L t (V v vf)) =
  norAgreement norCons (L t (V v (norkConcord norkF vf)))
norNorkAgreement (L _ (N _ norkF)) norCons (L t (V v vf)) =
  norAgreement norCons (L t (V v (norkConcord norkF vf)))

module Neo4J.Queries
  ( getNodes,
    getNodePair,
    anthroponymsQ,
    occupationsQ,
    personIntensiveQ,
    norNorkVerbsQ,
    possessionQ,
  )
where

import Control.Applicative (liftA2)
import Control.Monad
import Data.List (intersperse)
import qualified Data.Map as M
import qualified Data.Text as T
import Database.Bolt
import EuskeraAppError
import Lexis
import Neo4J.Conversion

type NodeRef = String

type CypherQuery = String

-- | Get all individual nodes matching a query and create Lexis elements
getNodes ::
  (NodeRef -> CypherQuery) ->
  BoltActionT IO [Either EuskeraAppError Lexis]
getNodes qry = do
  let nodeRef = "n"
  records <- query $ T.pack (qry nodeRef)
  forM records $ \record ->
    nodeToLexis
      <$> (record `at` T.pack nodeRef)

-- | Create pairs of Lexis elements from query
getNodePair ::
  ([NodeRef] -> CypherQuery) ->
  BoltActionT IO [Either EuskeraAppError (Lexis, Lexis)]
getNodePair qry = do
  let ref1 = "r1"
  let ref2 = "r2"
  records <- query $ T.pack (qry [ref1, ref2])
  firsts <- forM records $ \record ->
    nodeToLexis <$> record `at` T.pack ref1
  seconds <- forM records $ \record ->
    nodeToLexis <$> record `at` T.pack ref2
  return $ zipWith makePair firsts seconds

makePair ::
  Either EuskeraAppError a ->
  Either EuskeraAppError b ->
  Either EuskeraAppError (a, b)
makePair (Left error) _ = Left error
makePair _ (Left error) = Left error
makePair (Right a) (Right b) = Right (a, b)

-- | Query to retrieve all person names
anthroponymsQ :: NodeRef -> CypherQuery
anthroponymsQ nodeRef =
  randomizeQueryResults
    nodeRef
    ( "MATCH ("
        ++ nodeRef
        ++ ":Anthroponym)"
    )

-- | Query to retrieve all jobs associated to a person
occupationsQ :: NodeRef -> CypherQuery
occupationsQ nodeRef =
  randomizeQueryResults
    nodeRef
    ( "MATCH (:Occupation)<-[:IDENTITY]-("
        ++ nodeRef
        ++ ":Noun)<-[:CLASS_ASCRIPTION]-(:Person)"
    )

-- | Query to retrieve all verb-object pairs where the object is the nor argument
-- of the verb, in random order.
norNorkVerbsQ :: [NodeRef] -> CypherQuery
norNorkVerbsQ [vRef, nRef] =
  "match ("
    ++ vRef
    ++ ":NorNork)-[:NOR]->(:Category)<-[:IDENTITY*0..]-(:Category)<-[:IDENTITY]-("
    ++ nRef
    ++ ":Noun) "
    ++ "with "
    ++ vRef
    ++ ", "
    ++ nRef
    ++ ", "
    ++ "rand() as r with "
    ++ vRef
    ++ ", "
    ++ nRef
    ++ ", "
    ++ "apoc.util.md5([id("
    ++ vRef
    ++ "), id("
    ++ nRef
    ++ "), r]) as hash "
    ++ "return "
    ++ vRef
    ++ ", "
    ++ nRef
    ++ " order by hash"

-- Query to retrieve all the nodes related to the category Person by the
-- relationship INTENSIVE
personIntensiveQ :: NodeRef -> CypherQuery
personIntensiveQ nodeRef =
  "call {match (:Person)-[:INTENSIVE]->(" ++ nodeRef ++ ") "
    ++ "with "
    ++ nodeRef
    ++ ", rand() as r return "
    ++ nodeRef
    ++ ", r "
    ++ "union all "
    ++ " match (:Person)<-[:IDENTITY]-()-[:INTENSIVE]-("
    ++ nodeRef
    ++ ") "
    ++ " with "
    ++ nodeRef
    ++ ", rand() as r return "
    ++ nodeRef
    ++ ", r} return "
    ++ nodeRef
    ++ " order by r"

-- | Query to retrieve all objects that can be possessed
-- by a person
possessionQ :: NodeRef -> CypherQuery
possessionQ nodeRef =
  "match (:Person)-[:OWNERSHIP]->(:Category)<-[:IDENTITY*0..]-("
    ++ nodeRef
    ++ ":Common) "
    ++ "with "
    ++ nodeRef
    ++ ", "
    ++ "rand() as r with "
    ++ nodeRef
    ++ ", "
    ++ "apoc.util.md5([id("
    ++ nodeRef
    ++ "), r]) as hash "
    ++ "return "
    ++ nodeRef
    ++ " order by hash"

-- | Given a MATCH expression, provides query results in random order
-- | Only works for queries that retrieve nodes from just 1 category
randomizeQueryResults :: NodeRef -> CypherQuery -> CypherQuery
randomizeQueryResults nodeRef query =
  unwords
    [ query,
      "WITH ",
      nodeRef,
      ", rand() as r",
      "order by r",
      "return ",
      nodeRef
    ]

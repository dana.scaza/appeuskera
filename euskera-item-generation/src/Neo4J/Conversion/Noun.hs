module Neo4J.Conversion.Noun
  ( createAnthroponym,
    createCommonNoun,
    createMassNoun,
  )
where

import BasicData
import qualified Data.Map as M
import qualified Data.Text as T
import Database.Bolt
import EuskeraAppError
import Features.Nominal
import Lexis
import Neo4J.Conversion.Utils

createAnthroponym :: Node -> Either EuskeraAppError Lexis
createAnthroponym node = Right $ Lexis.N (Noun $ Anthroponym root) baseNounF
  where
    root = getRoot node

createCommonNoun :: Node -> Either EuskeraAppError Lexis
createCommonNoun node =
  Right $
    Lexis.N
      (Noun $ Count root defaultNumber)
      (baseNounF {number = getNumber node})
  where
    root = getRoot node
    defaultNumber = getNumber node

createMassNoun :: Node -> Either EuskeraAppError Lexis
createMassNoun node =
  Right $
    Lexis.N
      (Noun $ Mass root)
      (baseNounF {number = Singular})
  where
    root = getRoot node

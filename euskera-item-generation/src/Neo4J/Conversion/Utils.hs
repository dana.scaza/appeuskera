module Neo4J.Conversion.Utils (conversionError, getNumber, getRoot) where

import qualified Data.Map as M
import qualified Data.Text as T
import Database.Bolt
import EuskeraAppError
import Features.Nominal
import Lexis

conversionError :: Node -> Either EuskeraAppError Lexis
conversionError node =
  Left $ ConversionError $ "Node id: " ++ show (nodeIdentity node)

-- | Retrieves root property from node
getRoot :: Node -> String
getRoot node = case M.lookup root properties of
  Nothing -> ""
  Just (T x) -> T.unpack x
  _ -> ""
  where
    properties = nodeProps node
    root = T.pack "root"

-- | Retrieves number from Node
-- If number property is  missing from Node,
-- Singular is assigned by default
getNumber :: Node -> Number
getNumber node =
  case M.lookup number properties of
    Nothing -> Singular --
    Just (T x) ->
      case T.unpack x of
        "Singular" -> Singular
        "Plural" -> Plural
        _ -> Singular
    _ -> Singular
  where
    properties = nodeProps node
    number = T.pack "defaultNumber"

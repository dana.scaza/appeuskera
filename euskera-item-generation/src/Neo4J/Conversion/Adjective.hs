module Neo4J.Conversion.Adjective (createQualitativeAdjective) where

import BasicData
import Database.Bolt
import EuskeraAppError
import Features.Nominal
import Lexis
import Neo4J.Conversion.Utils

createQualitativeAdjective :: Node -> Either EuskeraAppError Lexis
createQualitativeAdjective node =
  Right $ Adj (Qualitative root) baseNounF
  where
    root = getRoot node

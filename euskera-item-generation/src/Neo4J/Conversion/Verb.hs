module Neo4J.Conversion.Verb (createNorNorkVerb) where

import BasicData
import Database.Bolt
import EuskeraAppError
import Features.Verbal
import Lexis
import Neo4J.Conversion.Utils

createNorNorkVerb :: Node -> Either EuskeraAppError Lexis
createNorNorkVerb node = Right $ V (NorNork root) baseVF
  where
    root = getRoot node

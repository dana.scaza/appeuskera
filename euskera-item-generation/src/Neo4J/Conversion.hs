module Neo4J.Conversion (nodeToLexis) where

import qualified Data.Text as T
import Database.Bolt
import EuskeraAppError
import Lexis
import Neo4J.Conversion.Adjective
import Neo4J.Conversion.Noun
import Neo4J.Conversion.Utils
import Neo4J.Conversion.Verb

-- | Use node information to create a Lexis of the appropriate type
-- with default features
nodeToLexis :: Node -> Either EuskeraAppError Lexis
nodeToLexis node
  | getRoot node == "" = conversionError node
  | "Anthroponym" `elem` wordclass = createAnthroponym node
  | "Common" `elem` wordclass = createCommonNoun node
  | "Mass" `elem` wordclass = createMassNoun node
  | "Adjective" `elem` wordclass = createQualitativeAdjective node
  | "NorNork" `elem` wordclass = createNorNorkVerb node
  | otherwise = Left Unimplemented
  where
    wordclass = T.unpack <$> labels node

{-# OPTIONS_HADDOCK hide, prune, ignore-exports #-}

module Neo4J.Config (myConfiguration) where

import Data.Default
import qualified Data.Text as T
import Database.Bolt

myConfiguration :: BoltCfg
myConfiguration =
  def
    { user = T.pack "neo4j",
      password = T.pack "euskera",
      host = "0.0.0.0"
    }

module Morphology.NominalForms (basePronominalForms, demonstrativeAbsolutive, basePossessiveForms) where

import Features
import Morphology.Utils

basePronominalForms :: Person -> Number -> String
basePronominalForms First Singular = "ni"
basePronominalForms Second Singular = "zu"
basePronominalForms Third Singular = "hura"
basePronominalForms First Plural = "gu"
basePronominalForms Second Plural = "zuek"
basePronominalForms Third Plural = "haiek"

demonstrativeAbsolutive :: Distance -> Number -> InflectedWord
demonstrativeAbsolutive Close Singular = "hau"
demonstrativeAbsolutive Medium Singular = "hori"
demonstrativeAbsolutive Far Singular = "hura"
demonstrativeAbsolutive Close Plural = "hauek"
demonstrativeAbsolutive Medium Plural = "horiek"
demonstrativeAbsolutive Far Plural = "haiek"

basePossessiveForms :: Person -> Number -> InflectedWord
basePossessiveForms First Singular = "nire"
basePossessiveForms Second Singular = "zure"
basePossessiveForms Third Singular = "haren"
basePossessiveForms First Plural = "gure"
basePossessiveForms Second Plural = "zuen"
basePossessiveForms Third Plural = "haien"

module Morphology.Inflection (inflect) where

import EuskeraAppError
import Features
import Lexis
import Morphology.Nominal
import Morphology.Utils
import Morphology.Verbal

-- | Lexis transformation into fully inflected word.
inflect :: Lexis -> Either EuskeraAppError InflectedWord
inflect (N noun nf) = inflectNominal nf noun
inflect (V verb vf) = inflectVerb vf verb
inflect (Adj adj nf) = inflectAdjective nf adj
inflect (Adv (A adv) advf) = Right adv

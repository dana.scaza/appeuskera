module Morphology.IrregularVerbs
  ( inflectIzan,
    inflectUkan,
    izanPresent,
    izanPast,
    ukanPresent,
    ukanPast,
  )
where

import EuskeraAppError
import Features
import Morphology.Utils

--------------------------------------------------------------------------------
-- IZAN ------------------------------------------------------------------------
--------------------------------------------------------------------------------

-- | Synthetic inflection of verb "izan" (to be)
inflectIzan :: Tense -> NF -> InflectedWord
inflectIzan Present norkFeatures =
  izanPresent
    (person norkFeatures)
    (number norkFeatures)
inflectIzan Past norkFeatures =
  izanPast
    (person norkFeatures)
    (number norkFeatures)

-- | Present punctual conjugation of verb "izan"
izanPresent :: Person -> Number -> InflectedWord
izanPresent First Singular = "naiz"
izanPresent Second Singular = "zara"
izanPresent Third Singular = "da"
izanPresent First Plural = "gara"
izanPresent Second Plural = "zarete"
izanPresent Third Plural = "dira"

-- | Past punctual conjugation of verb "izan"
izanPast :: Person -> Number -> InflectedWord
izanPast First Singular = "nintzen"
izanPast Second Singular = "zinen"
izanPast Third Singular = "zen"
izanPast First Plural = "ginen"
izanPast Second Plural = "zineten"
izanPast Third Plural = "ziren"

--------------------------------------------------------------------------------
-- UKAN ------------------------------------------------------------------------
--------------------------------------------------------------------------------

-- | Synthetic inflection of verb "ukan" (to have)
inflectUkan ::
  Tense ->
  (Person, Number) ->
  (Person, Number) ->
  Either EuskeraAppError InflectedWord
inflectUkan _ (First, _) (First, _) =
  Left $ InflectionError "Nor and nor arguments cannot both be first person"
inflectUkan _ (Second, _) (Second, _) =
  Left $ InflectionError "Nor and nor arguments cannot both be second person"
inflectUkan Present norkF norF = Right $ ukanPresent norkF norF
inflectUkan Past norkF norF = Right $ ukanPast norkF norF

-- | Present punctual conjugation of verb "ukan"
ukanPresent ::
  (Person, Number) ->
  (Person, Number) ->
  InflectedWord
ukanPresent (Third, Plural) norPersonNum
  | norPersonNum == (First, Singular) =
    norkThirdPluralException (ukanNorMorphemePresent norPersonNum)
  | norPersonNum == (Third, Plural) =
    norkThirdPluralException (ukanNorMorphemePresent norPersonNum)
  | norPersonNum == (Second, Plural) =
    norkThirdPluralException (ukanNorMorphemePresent norPersonNum)
ukanPresent norkPersonNum norPersonNum =
  ukanNorkMorphemePresent norkPersonNum (ukanNorMorphemePresent norPersonNum)

ukanNorMorphemePresent :: (Person, Number) -> String
ukanNorMorphemePresent (First, Singular) = "nau"
ukanNorMorphemePresent (Second, Singular) = "zaitu"
ukanNorMorphemePresent (Third, Singular) = "du"
ukanNorMorphemePresent (First, Plural) = "gaitu"
ukanNorMorphemePresent (Second, Plural) = "zaituzte"
ukanNorMorphemePresent (Third, Plural) = "ditu"

ukanNorkMorphemePresent :: (Person, Number) -> String -> String
ukanNorkMorphemePresent (First, Singular) = (++ "t")
ukanNorkMorphemePresent (Second, Singular) = (++ "zu")
ukanNorkMorphemePresent (Third, Singular) = id
ukanNorkMorphemePresent (First, Plural) = (++ "gu")
ukanNorkMorphemePresent (Second, Plural) = (++ "zue")
ukanNorkMorphemePresent (Third, Plural) = (++ "te")

norkThirdPluralException :: String -> String
norkThirdPluralException = (++ "zte")

-- | Present punctual conjugation of verb "ukan"
ukanPast ::
  (Person, Number) ->
  (Person, Number) ->
  InflectedWord
ukanPast norkF (Third, norNum) = ukanThirdPersonPast norNum norkF
ukanPast norkF norF = ukanNorkMorphemePast norkF (ukanNorMorphemePast norF)

ukanNorMorphemePast :: (Person, Number) -> String
ukanNorMorphemePast (First, Singular) = "nindu"
ukanNorMorphemePast (Second, Singular) = "zintu"
ukanNorMorphemePast (First, Plural) = "gintu"
ukanNorMorphemePast (Second, Plural) = "zintuzte"

ukanNorkMorphemePast :: (Person, Number) -> String -> String
ukanNorkMorphemePast (First, Singular) = (++ "dan")
ukanNorkMorphemePast (Second, Singular) = (++ "zun")
ukanNorkMorphemePast (Third, Singular) = (++ "en")
ukanNorkMorphemePast (First, Plural) = (++ "gun")
ukanNorkMorphemePast (Second, Plural) = (++ "zuen")
ukanNorkMorphemePast (Third, Plural) = (++ "ten")

ukanThirdPersonPast :: Number -> (Person, Number) -> String
ukanThirdPersonPast norNum (Second, Plural) =
  case norNum of
    Singular -> "zenuten"
    Plural -> "zenituzten"
ukanThirdPersonPast norNum (Third, Plural) =
  case norNum of
    Singular -> "zuten"
    Plural -> "zituzten"
ukanThirdPersonPast norNumber (norkPerson, norkNumber)
  | norNumber == Singular =
    ukanThirdPersonPastPattern (norkPerson, norkNumber) ""
  | norNumber == Plural =
    ukanThirdPersonPastPattern (norkPerson, norkNumber) "it"

ukanThirdPersonPastPattern :: (Person, Number) -> String -> String
ukanThirdPersonPastPattern (First, Singular) plural = "n" ++ plural ++ "uen"
ukanThirdPersonPastPattern (Second, Singular) plural = "zen" ++ plural ++ "uen"
ukanThirdPersonPastPattern (Third, Singular) plural = "z" ++ plural ++ "uen"
ukanThirdPersonPastPattern (First, Plural) plural = "gen" ++ plural ++ "uen"

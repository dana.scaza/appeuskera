module Morphology.Nominal (inflectNominal, inflectAdjective) where

import EuskeraAppError
import Features
import Lexis
import Morphology.NominalForms
import Morphology.Utils

inflectNominal :: NF -> Nominal -> Either EuskeraAppError InflectedWord
inflectNominal nf EmptyNoun = Right ""
inflectNominal nf Pronoun
  | possessive nf /= NoPoss = inflectPossPronoun nf
  | determiner nf /= Definite = inflectDemPronoun nf
  | otherwise = inflectPronoun nf
inflectNominal nf (Noun n)
  | getNoun n == "" = Left $ EmptyString "inflectNoun: Empty proper noun"
  | otherwise = inflectNoun nf n

--------------------------------------------------------------------------------
-- Pronouns
--------------------------------------------------------------------------------

inflectPossPronoun :: NF -> Either EuskeraAppError InflectedWord
inflectPossPronoun nf = Right $ basePossessiveForms (person nf) (number nf)

inflectDemPronoun :: NF -> Either EuskeraAppError InflectedWord
inflectDemPronoun nf = Right "unimplemented dem pronoun"

inflectPronoun :: NF -> Either EuskeraAppError InflectedWord
inflectPronoun nf
  | grammCase nf == Absolutive =
    Right $
      absolutivePronoun
        (person nf)
        (number nf)
  | grammCase nf == Ergative =
    Right $
      ergativePronoun
        (person nf)
        (number nf)
  | grammCase nf == Dative =
    Right $
      dativePronoun
        (person nf)
        (number nf)
inflectPronoun nf = case grammCase nf of
  (Genitive p n Nothing) -> Right $ basePossessiveForms p n
  (Genitive p n (Just Absolutive)) -> Right $ basePossessiveForms p n ++ "a"
  _ -> error "unimplemented genitive pronoun declination"

absolutivePronoun :: Person -> Number -> InflectedWord
absolutivePronoun = basePronominalForms

ergativePronoun :: Person -> Number -> InflectedWord
ergativePronoun Third Singular = "hark"
ergativePronoun p n = ifEndsInK id (++ "k") $ basePronominalForms p n

dativePronoun :: Person -> Number -> InflectedWord
dativePronoun p n =
  ifEndsInK
    (\pro -> init pro ++ "i")
    (++ "ri")
    $ basePronominalForms p n

--------------------------------------------------------------------------------
-- Nouns
--------------------------------------------------------------------------------
inflectNoun :: NF -> Noun -> Either EuskeraAppError InflectedWord
inflectNoun nf (Proper noun) = inflectProper nf noun
inflectNoun nf (Anthroponym noun) = inflectProper nf noun
inflectNoun nf (Count noun num) = inflectCommon nf noun
inflectNoun nf (Mass noun) = inflectCommon nf noun

inflectAdjective :: NF -> Adjective -> Either EuskeraAppError InflectedWord
inflectAdjective nf (Qualitative adj) = inflectCommon nf adj
inflectAdjective nf (Relative adj) = inflectCommon nf adj

inflectCommon :: NF -> Lexeme -> Either EuskeraAppError InflectedWord
inflectCommon nf noun
  | grammCase nf == Absolutive = inflectCommonAbsolutive nf noun
  | grammCase nf == Ergative = Right $ noun ++ "(unimpl)"
  | grammCase nf == Dative = Right $ noun ++ "(unimpl)"

inflectProper :: NF -> Lexeme -> Either EuskeraAppError InflectedWord
inflectProper nf noun
  | grammCase nf == Absolutive = absolutiveUndetermined noun
  | grammCase nf == Ergative = ergativeUndetermined noun
  | grammCase nf == Dative = dativeUndetermined noun
inflectProper nf noun =
  case grammCase nf of
    (Genitive p n Nothing) -> Right genitiveNoun
    (Genitive p n (Just Absolutive)) ->
      case number nf of
        Singular -> Right (genitiveNoun ++ "a")
        Plural -> Right (genitiveNoun ++ "ak")
    _ -> error "unimplemented proper declination"
  where
    genitiveNoun = genitiveUndetermined noun

inflectCommonAbsolutive :: NF -> Lexeme -> Either EuskeraAppError InflectedWord
inflectCommonAbsolutive nf noun =
  case determiner nf of
    Definite -> absolutiveDefinite nf noun
    Indefinite -> absolutiveIndefinite nf noun
    Dem dist -> absolutiveDemonstrative dist (number nf) noun

absolutiveDefinite :: NF -> Lexeme -> Either EuskeraAppError InflectedWord
absolutiveDefinite nf =
  Right . addPluralSuffix (number nf) . addBaseSuffix
  where
    addPluralSuffix Singular = id
    addPluralSuffix Plural = (++ "k")
    addBaseSuffix = ifEndsInA id (ifEndsInR (++ "ra") (++ "a"))

absolutiveUndetermined :: Lexeme -> Either EuskeraAppError InflectedWord
absolutiveUndetermined = Right

absolutiveDemonstrative :: Distance -> Number -> Lexeme -> Either EuskeraAppError InflectedWord
absolutiveDemonstrative dist num noun = Right $ noun ++ " " ++ demonstrativeAbsolutive dist num

absolutiveIndefinite :: NF -> Lexeme -> Either EuskeraAppError InflectedWord
absolutiveIndefinite nf noun = case number nf of
  Singular -> Right $ noun ++ " bat"
  Plural -> Right $ noun ++ " batzuk"

ergativeUndetermined :: Lexeme -> Either EuskeraAppError InflectedWord
ergativeUndetermined =
  Right . ifEndsInVowel (++ "k") (ifEndsInR (++ "rek") (++ "ek"))

dativeUndetermined :: Lexeme -> Either EuskeraAppError InflectedWord
dativeUndetermined =
  Right . ifEndsInVowel (++ "ri") (ifEndsInR (++ "ri") (++ "i"))

genitiveUndetermined :: Lexeme -> InflectedWord
genitiveUndetermined = ifEndsInVowel (++ "ren") (ifEndsInR (++ "ren") (++ "en"))

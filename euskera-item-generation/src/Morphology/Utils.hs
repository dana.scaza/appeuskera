module Morphology.Utils
  ( Lexeme,
    InflectedWord,
    Suffix,
    ifEndsInVowel,
    ifEndsInA,
    ifEndsInK,
    ifEndsInN,
    ifEndsInR,
    endsInSuffixes,
  )
where

import Data.List

type Lexeme = String

type InflectedWord = String

type Suffix = String

vowels :: [String]
vowels = ["a", "e", "i", "o", "u"]

-- | If given word has indicated ending, apply first function; otherwise,
--  apply second function
ifEndsInVowel,
  ifEndsInA,
  ifEndsInK,
  ifEndsInN,
  ifEndsInR ::
    (Lexeme -> InflectedWord) ->
    (Lexeme -> InflectedWord) ->
    Lexeme ->
    InflectedWord
ifEndsInVowel optionA optionB word =
  if endsInVowel word
    then optionA word
    else optionB word
ifEndsInA = endingBasedInflection "a"
ifEndsInK = endingBasedInflection "k"
ifEndsInR = endingBasedInflection "r"
ifEndsInN = endingBasedInflection "n"

-- | Given a list of suffixes and a word, checks whether the word
-- ends in any of the suffixes
endsInSuffixes :: [Suffix] -> Lexeme -> Bool
endsInSuffixes suffixes word = any ($ word) suffixEndings
  where
    suffixEndings = map isSuffixOf suffixes

endsInVowel :: Lexeme -> Bool
endsInVowel = endsInSuffixes vowels

-- if ends in X, do P, else Q
-- if prefix is empty, it will be optionA
endingBasedInflection ::
  Suffix ->
  (Lexeme -> InflectedWord) ->
  (Lexeme -> InflectedWord) ->
  Lexeme ->
  InflectedWord
endingBasedInflection _ _ _ "" = ""
endingBasedInflection suffix optionA optionB word =
  if suffix `isSuffixOf` word
    then optionA word
    else optionB word

module Morphology.Verbal (inflectVerb) where

import Data.List
import EuskeraAppError
import Features
import Lexis
import Morphology.IrregularVerbs
import Morphology.Utils

inflectVerb :: VF -> Verb -> Either EuskeraAppError InflectedWord
inflectVerb vf v
  | getVerb v == "" = Left $ EmptyString "verb is empty"
  | instantiation vf == Full = inflectFullVerb vf v
  | instantiation vf == Main = inflectLexicalVerb vf v
  | instantiation vf == AuxOrTrinko = inflectAuxOrTrinko vf v

inflectFullVerb,
  inflectLexicalVerb,
  inflectAuxOrTrinko ::
    VF ->
    Verb ->
    Either EuskeraAppError InflectedWord
inflectFullVerb vf v
  | aspect vf == Punctual = inflectIrregularVerbs vf v
  | otherwise =
    unwords
      <$> sequenceA
        [ inflectAspect
            (aspect vf)
            (getVerb v),
          chooseAux vf v
        ]
inflectLexicalVerb vf v
  | aspect vf == Punctual = Right ""
  | otherwise = inflectAspect (aspect vf) (getVerb v)
inflectAuxOrTrinko vf v
  | aspect vf == Punctual = inflectIrregularVerbs vf v
  | otherwise = chooseAux vf v

--------------------------------------------------------------------------------
-- Aspectual inflection
--------------------------------------------------------------------------------
inflectAspect :: Aspect -> Lexeme -> Either EuskeraAppError InflectedWord
inflectAspect Future = future
inflectAspect Imperfective = imperfective
inflectAspect Perfective = perfective
inflectAspect _ =
  \x ->
    Left $
      InflectionError
        "inflectAspect expects Future, Imperfective or Perfective"

future :: Lexeme -> Either EuskeraAppError InflectedWord
future = Right . ifEndsInVowel (++ "ko") (++ "go")

imperfective :: Lexeme -> Either EuskeraAppError InflectedWord
imperfective verb
  | endsIn "tsi" = Right $ strip3 ++ "sten"
  | endsIn "txi" = Right $ strip3 ++ "xten"
  | endsIn "tzi" = Right $ strip3 ++ "zten"
  | endsInTenEndings = Right $ strip1 ++ "ten"
  | endsIn "i" = Right $ verb ++ "tzen"
  | endsInTzenEndings = Right $ strip2 ++ "tzen"
  | otherwise = Left $ InflectionError "unknown verb ending"
  where
    endsIn s = s `isSuffixOf` verb
    endsInTenEndings = endsInSuffixes ["n", "si", "xi", "zi"] verb
    endsInTzenEndings = endsInSuffixes ["du", "tu"] verb
    strip1 = init verb
    strip2 = (init . init) verb
    strip3 = (init . init . init) verb

perfective :: Lexeme -> Either EuskeraAppError InflectedWord
perfective = Right

--------------------------------------------------------------------------------
-- Auxiliary verb inflection
--------------------------------------------------------------------------------

chooseAux :: VF -> Verb -> Either EuskeraAppError InflectedWord
chooseAux vf (Nor _) = norAux (tense vf) (nor vf)
chooseAux vf (Copula _) = norAux (tense vf) (nor vf)
chooseAux vf (NorNork _) = norNorkAux (tense vf) (nork vf) (nor vf)
chooseAux _ _ = Left Unimplemented

norAux :: Tense -> NF -> Either EuskeraAppError InflectedWord
norAux Present nf = Right $ izanPresent (person nf) (number nf)
norAux Past nf = Right $ izanPast (person nf) (number nf)

norNorkAux :: Tense -> NF -> NF -> Either EuskeraAppError InflectedWord
norNorkAux tense nork nor =
  case tense of
    Present -> Right $ ukanPresent norkF norF
    Past -> Right $ ukanPast norkF norF
  where
    norkF = personNumber nork
    norF = personNumber nor

--------------------------------------------------------------------------------
-- Irregular verb inflection
--------------------------------------------------------------------------------
inflectIrregularVerbs :: VF -> Verb -> Either EuskeraAppError InflectedWord
inflectIrregularVerbs vf (Nor "izan") = Right $ inflectIzan (tense vf) (nor vf)
inflectIrregularVerbs vf (Copula "izan") = Right $ inflectIzan (tense vf) (nor vf)
inflectIrregularVerbs vf (NorNork "ukan") =
  inflectUkan (tense vf) (personNumber $ nork vf) (personNumber $ nor vf)
inflectIrregularVerbs _ _ = Left Unimplemented

-- https://www.hiru.eus/es/lengua-vasca/aspecto-tiempo-y-modo

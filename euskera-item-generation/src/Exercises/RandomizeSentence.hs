-- |
-- Module      : RandomizeSentence
--
-- This module contains all the functions that generate random objects and work
-- with the 'SentenceState' in the State monad.
module Exercises.RandomizeSentence
  ( randomPersonNumberThirdSingBias,
    randomPersonNumber,
    randomNumber,
    randomDeterminer,
    randomDeterminerDegree,
    randomizeSentenceType,
  )
where

import BasicData
import Control.Monad.State
import Data.List
import Features
import Syntax.ProtoSentence
import System.Random

randomRange :: (Int, Int)
randomRange = (0, 100)

-- The Haskell type State describes functions that consume a state and
-- produce both a result and an updated state, which are given back in a tuple.
randomizeSentenceWith ::
  (Int -> ProtoSentence -> ProtoSentence) ->
  State (StdGen, ProtoSentence) ProtoSentence
randomizeSentenceWith f = state $ \(gen, proto) ->
  let (randomNum, newGen) = randomR randomRange gen
      newSent = f randomNum proto
   in (newSent, (newGen, newSent))

-- | Generatos an 'NF' with a random 'Person' and 'Number' and absolutive 'Case'
randomPersonNumber :: State (StdGen, ProtoSentence) NF
randomPersonNumber = state $ \(gen, proto) ->
  let (randomNum, newGen) = randomR randomRange gen
      (p, n) = personNumberOptions !! (randomNum `mod` length personNumberOptions)
   in (norArgF {person = p, number = n}, (newGen, proto))

-- | Generates an 'NF' with a random 'Person' and 'Number' and absolutive 'Case'.
-- The probability of (Third, Singular) is higher than the rest of the combinations.
randomPersonNumberThirdSingBias :: State (StdGen, ProtoSentence) NF
randomPersonNumberThirdSingBias = state $ \(gen, proto) ->
  let (randomNum, newGen) = randomR randomRange gen
      (p, n) = personNumberOptionsThirdSingBias !! (randomNum `mod` length personNumberOptions)
   in (norArgF {person = p, number = n}, (newGen, proto))

-- | Produces a random grammatical 'Number': Singular or Plural
randomNumber :: State (StdGen, ProtoSentence) Number
randomNumber = state $ \(gen, proto) ->
  let (randomNum, newGen) = randomR (0, 1) gen
      num = [Singular, Plural] !! randomNum
   in (num, (newGen, proto))

-- | Produces a random 'Determiner'
randomDeterminer :: State (StdGen, ProtoSentence) Determiner
randomDeterminer = state $ \(gen, proto) ->
  let (randomNum, newGen) = randomR randomRange gen
      det = determinerOptions !! (randomNum `mod` length determinerOptions)
   in (det, (newGen, proto))

-- | Produces a random determiner 'Distance' degree
randomDeterminerDegree :: State (StdGen, ProtoSentence) Distance
randomDeterminerDegree = state $ \(gen, proto) ->
  let (randomNum, newGen) = randomR randomRange gen
      deg = determinerDegreeOptions !! (randomNum `mod` length determinerDegreeOptions)
   in (deg, (newGen, proto))

-- | Randomizes the type of the 'ProtoSentence' contained in the 'SentenceState'
randomizeSentenceType :: State (StdGen, ProtoSentence) ProtoSentence
randomizeSentenceType = randomizeSentenceWith randomSentenceType

--------------------------------------------------------------------------------
-- Helpers
--------------------------------------------------------------------------------

randomSentenceType :: Int -> ProtoSentence -> ProtoSentence
randomSentenceType n proto = proto {sentType = t, negated = b}
  where
    (t, b) = sentOptions !! (n `mod` length sentOptions)

-- We want most sentences to be declarative and non negative
sentenceTypes = [Declarative, Declarative, YesNoInterrogative]

negatedOrNot = [False, False, True]

sentOptions = [(s, n) | s <- sentenceTypes, n <- negatedOrNot]

personNumberOptions =
  [ (p, n)
    | p <- [First, Second, Third],
      n <- [Singular, Plural]
  ]

personNumberOptionsThirdSingBias = replicate 2 (Third, Singular) ++ personNumberOptions

determinerOptions = [Definite, Indefinite, Dem Close, Dem Medium, Dem Far]

determinerDegreeOptions = [Close, Medium, Far]

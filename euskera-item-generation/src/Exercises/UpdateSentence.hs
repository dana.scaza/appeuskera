-- |
-- Module      : UpdateSentence
--
-- This module contains all the functions that update the 'SentenceState' within
-- the State monad without randomization
module Exercises.UpdateSentence
  ( initializeCopula,
    initializeNorNork,
    updateTenseAndAspect,
    updatePronominalSubjectAndAttribute,
    updateSubjectAndAttribute,
    updateVerb,
    updateNorWithNP,
    updateNorkWithPronoun,
    updateAttributeTagsWith,
    updateSubjectTagsWith,
  )
where

import Control.Monad.State
import Features
import Lexis
import Syntax
import System.Random

updateStateWith ::
  (ProtoSentence -> ProtoSentence) ->
  State (StdGen, ProtoSentence) ProtoSentence
updateStateWith f = state $ \(gen, proto) ->
  let newSent = f proto in (newSent, (gen, newSent))

updateTenseAndAspect ::
  Tense ->
  Aspect ->
  State (StdGen, ProtoSentence) ProtoSentence
updateTenseAndAspect tense aspect = updateStateWith (assignTenseAndAspect tense aspect)

updateVerb :: Lexis -> State (StdGen, ProtoSentence) ProtoSentence
updateVerb verb = updateStateWith (modifyVerb (getVerbFromLexis verb))

-- | Update the nork constituent with a 'Pronoun'
updateNorkWithPronoun :: NF -> State (StdGen, ProtoSentence) ProtoSentence
updateNorkWithPronoun nf = updateStateWith (assignPronominalNork nf)

-- | Update the nor constituent with an 'NPConstituent'
updateNorWithNP :: NPConstituent -> State (StdGen, ProtoSentence) ProtoSentence
updateNorWithNP np = updateStateWith (modifyNor np)

-- | Update the Subject (nor constituent) with a 'Pronoun' and the
-- attribute with the given 'Lexis' pair.
updatePronominalSubjectAndAttribute ::
  NF ->
  -- | A pair of person names to fill the subject position
  (Lexis, Lexis) ->
  State (StdGen, ProtoSentence) ProtoSentence
updatePronominalSubjectAndAttribute nf nouns =
  updateStateWith (assignPronominalSubjectAndAttribute nf nouns)

-- | Update the Subject (nor constituent) and the attribute with the given
-- 'Lexis' elements
updateSubjectAndAttribute ::
  -- | Nor constituent
  Lexis ->
  -- | Attribute
  Lexis ->
  State (StdGen, ProtoSentence) ProtoSentence
updateSubjectAndAttribute subj attr =
  updateStateWith (assignSubjectAndAttribute subj attr)

-- | Add the given tag to the attribute constituent
updateAttributeTagsWith :: Tag -> State (StdGen, ProtoSentence) ProtoSentence
updateAttributeTagsWith tag =
  updateStateWith (addTagToAttribute tag)

-- | Add the given tag to the subject constituent
updateSubjectTagsWith :: Tag -> State (StdGen, ProtoSentence) ProtoSentence
updateSubjectTagsWith tag =
  updateStateWith (addTagToSubject tag)

-- | Initialize the 'ProtoSentence' in the state with copula compatible constituents
-- and features
initializeCopula :: State (StdGen, ProtoSentence) ProtoSentence
initializeCopula = updateStateWith startCopula

-- | Initialize the 'ProtoSentence' in the state with 'NorNork' compatible constituents
-- and features
initializeNorNork :: State (StdGen, ProtoSentence) ProtoSentence
initializeNorNork = updateStateWith startNorNork

--------------------------------------------------------------------------------
-- Helpers
--------------------------------------------------------------------------------
assignPronominalSubjectAndAttribute ::
  NF ->
  -- | A pair of person names to fill the subject position
  (Lexis, Lexis) ->
  ProtoSentence ->
  ProtoSentence
assignPronominalSubjectAndAttribute f (n1, n2) =
  modifyAttribute attr . modifyNor pronoun
  where
    pronoun = L [] (N Pronoun f)
    attr = case number f of
      Singular -> np n1 f
      Plural -> makePluralAttribute f n1 n2

assignSubjectAndAttribute ::
  -- | Subject
  Lexis ->
  -- | Attribute
  Lexis ->
  ProtoSentence ->
  ProtoSentence
assignSubjectAndAttribute s a =
  modifyAttribute (L [] a) . modifyNor (L [] s)

assignTenseAndAspect :: Tense -> Aspect -> ProtoSentence -> ProtoSentence
assignTenseAndAspect t a = modifyAspect a . modifyTense t

assignPronominalNork :: NF -> ProtoSentence -> ProtoSentence
assignPronominalNork nf = modifyNork (L ["nork"] $ N Pronoun (nf {grammCase = Ergative}))

makePluralAttribute :: NF -> Lexis -> Lexis -> Constituent
makePluralAttribute f n1 n2
  | isAnthroponym n1 && isAnthroponym n2 =
    coordNP
      (np n1 f {number = Singular})
      (np n2 f {number = Singular})
  | isAnthroponym n1 = np n2 f
  | otherwise = np n1 f

startCopula :: ProtoSentence -> ProtoSentence
startCopula _ = protoCopula

startNorNork :: ProtoSentence -> ProtoSentence
startNorNork _ = protoNorNork

addTagToAttribute :: Tag -> ProtoSentence -> ProtoSentence
addTagToAttribute tag proto = proto {attribute = addTag tag (attribute proto)}

addTagToSubject :: Tag -> ProtoSentence -> ProtoSentence
addTagToSubject tag proto = proto {norCons = addTag tag (norCons proto)}

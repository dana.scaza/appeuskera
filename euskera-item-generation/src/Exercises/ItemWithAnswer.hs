module Exercises.ItemWithAnswer
  ( ItemWithAnswer (..),
    getItem,
    getAnswer,
    addSimpleGaps,
    mainVerbHint,
    possessiveHint,
    Hint,
  )
where

import Data.Either
import Features
import Lexis
import Morphology.Inflection
import Morphology.NominalForms
import Syntax

-- | An item with answer is a tuple that contains:
--
-- * a sentential 'Constituent' representing the item, and
--
-- * some extra information regarding the answer (parametric argument)
newtype ItemWithAnswer a = ItemWithAnswer (Constituent, a)
  deriving (Show)

getItem :: ItemWithAnswer a -> Constituent
getItem (ItemWithAnswer (a, s)) = a

getAnswer :: ItemWithAnswer a -> a
getAnswer (ItemWithAnswer (a, s)) = s

-- | Given a 'Tag' and a 'Constituent', generate an 'ItemWithAnswer'
-- with the corresponding gapped version
addSimpleGaps :: Tag -> Constituent -> ItemWithAnswer [Maybe Constituent]
addSimpleGaps tag (L tags lexis)
  | tag `elem` tags = ItemWithAnswer (createGap (L tags lexis), [Just (L tags lexis)])
  | otherwise = ItemWithAnswer (L tags lexis, [Nothing])
addSimpleGaps tag c
  | tag `elem` getTags c = ItemWithAnswer (createGap c, [Just c])
  | otherwise = ItemWithAnswer (setConstituents c constituents, answer)
  where
    subitems = map (addSimpleGaps tag) (getConstituents c)
    constituents = map getItem subitems
    answer = concatMap getAnswer subitems

-- | A function to generate a hint to accompany an item
type Hint = [Maybe Constituent] -> String

-- | Hint to show along with the item that indicates which verb
-- should be conjugated
mainVerbHint :: Hint
mainVerbHint [] = error "Could not find main verb in gap list"
mainVerbHint ((Just (L tags (V verb _))) : xs) =
  if tags == ["mainVerb"]
    then " (" ++ getVerb verb ++ ")"
    else mainVerbHint xs
mainVerbHint (x : xs) = mainVerbHint xs

-- | Hint to show along with the item that indicates
-- the noun that should be inflected with genitive case
possessiveHint :: Hint
possessiveHint [] = error "Could not find noun in gap list"
possessiveHint ((Just (L tags (N Pronoun nf))) : xs) =
  " (" ++ basePronominalForms pers num ++ ")"
  where
    (pers, num) = case grammCase nf of
      (Genitive p n _) -> (p, n)
      _ -> let Poss p n = possessive nf in (p, n)
possessiveHint ((Just (L tags (N (Noun n) nf))) : xs) = " (" ++ getNoun n ++ ")"
possessiveHint (x : xs) = possessiveHint xs

-- | Given a constituent, create its corresponding FillGap constituent
createGap ::
  -- | The constituent that will be replaced by a gap
  Constituent ->
  -- | The FillGap constituent
  Constituent
createGap c =
  if instantiate c == Right ""
    then FillGap False
    else FillGap True

-- |
-- Module      : Exercises.Generation
--
-- This module contains the functions that need to be called to
-- generate an exercise
module Exercises.Generation
  ( SentenceState,
    copulaItemsPresent,
    copulaItemsPast,
    nor3NorkItems,
    possessiveItems,
  )
where

import BasicData
import Control.Monad.State.Lazy
import Data.Either
import Data.List
import Database.Bolt
import EuskeraAppError
import Exercises.ItemWithAnswer
import Exercises.RandomizeSentence
import Exercises.UpdateSentence
import Features
import Lexis
import Morphology.Inflection
import Neo4J
import Syntax
import System.Random

-- | The state that the State monad works with when generating exercises
type SentenceState = (StdGen, ProtoSentence)

--------------------------------------------------------------------------------
-- Single item generation
--------------------------------------------------------------------------------
copulaItem ::
  Tense ->
  Aspect ->
  (Lexis, Lexis) ->
  State SentenceState ProtoSentence
copulaItem t a nouns =
  do
    initializeCopula
    randomizeSentenceType
    updateTenseAndAspect t a
    nf <- randomPersonNumber
    updatePronominalSubjectAndAttribute nf nouns

possessiveItem :: Lexis -> Lexis -> State SentenceState ProtoSentence
possessiveItem object name =
  do
    initializeCopula
    randomizeSentenceType
    updateTenseAndAspect Present Punctual
    deg <- randomDeterminerDegree
    nf <- randomPersonNumberThirdSingBias
    let gen = Genitive (person nf) (number nf) (Just Absolutive)
    let subjectNP = modifyDeterminer object (Dem deg)
    let attrNP = pronounOrName (nf {grammCase = gen, number = getNounNumber name}) (modifyCase gen name)
    updateSubjectAndAttribute subjectNP attrNP
    updateAttributeTagsWith "possessive"

-- hau nire liburua da
possessiveDeterminerItem :: Lexis -> State SentenceState ProtoSentence
possessiveDeterminerItem object =
  do
    initializeCopula
    randomizeSentenceType
    updateTenseAndAspect Present Punctual
    deg <- randomDeterminerDegree
    nf <- randomPersonNumber
    let subjectNP = Lexis.N Pronoun (norArgF {determiner = Dem deg}) -- TODO: attribute's number
    let attrNP = modifyPossessive (Poss (person nf) (number nf)) object
    updateSubjectAndAttribute subjectNP attrNP
    updateSubjectTagsWith "possessive"

-- | 'NorNork' item limiting the Nor argument to 3rd person
nor3NorkItem ::
  Tense -> Aspect -> (Lexis, Lexis) -> State SentenceState ProtoSentence
nor3NorkItem t a (verb, noun) =
  do
    initializeNorNork
    updateVerb verb
    randomizeSentenceType
    updateTenseAndAspect t a
    nf <- randomPersonNumber
    updateNorkWithPronoun nf
    num <- randomNumber
    det <- randomDeterminer
    let norNP = nounToNPWithDetAndNum det num noun
    updateNorWithNP norNP

--------------------------------------------------------------------------------
-- Exercises
--------------------------------------------------------------------------------

-- | Generate n  'Copula' items in the indicated tense and execute exercises on console
copulaItemsPresent,
  copulaItemsPast ::
    -- | n: number of items to generate
    Int ->
    -- | a Hasbolt pipe to communicate with the Neo4J db
    Pipe ->
    SentenceState ->
    IO SentenceState
copulaItemsPresent n pipe s = do
  nameNodes <- run pipe $ getNodes personIntensiveQ :: IO [Either EuskeraAppError Lexis]
  let namePairs = listToPairs $ take (n * 2) $ cycle (rights nameNodes)
  let (sents, newState) = runState (mapM (copulaItem Present Punctual) namePairs) s
  --let items = map (addSimpleGaps "mainVerb" . createSentence) sents
  showItems "mainVerb" (createSentence <$> sents)
  return newState
copulaItemsPast n pipe s = do
  nameNodes <- run pipe $ getNodes occupationsQ :: IO [Either EuskeraAppError Lexis]
  let namePairs = listToPairs $ take (n * 2) $ cycle (rights nameNodes)
  let (sents, newState) = runState (mapM (copulaItem Past Punctual) namePairs) s
  --let items = map (addSimpleGaps "mainVerb" . createSentence) sents
  showItems "mainVerb" (createSentence <$> sents)
  return newState

-- | Generate n items with 'NorNork' verbs, in the given tense and aspect
nor3NorkItems ::
  -- | n: number of items to generate
  Int ->
  -- | a Hasbolt pipe to communicate with the Neo4J db
  Pipe ->
  -- | Tense of the items
  Tense ->
  -- | Aspect of the items
  Aspect ->
  SentenceState ->
  IO SentenceState
nor3NorkItems n pipe t a s = do
  dbVerbObjectPairs <-
    run pipe $
      getNodePair norNorkVerbsQ ::
      IO [Either EuskeraAppError (Lexis, Lexis)]
  let verbObjectPairs = take n $ cycle (rights dbVerbObjectPairs)
  let (sents, newState) = runState (mapM (nor3NorkItem t a) verbObjectPairs) s
  showItemsWithHint mainVerbHint "mainVerb" (createSentence <$> sents)
  return newState

-- | Generate n items using copulative structures and a possessive forms
-- (genitive case) in the attribute position.
possessiveItems ::
  -- | n: number of items to generate
  Int ->
  -- | a Hasbolt pipe to communicate with the Neo4J db
  Pipe ->
  SentenceState ->
  IO SentenceState
possessiveItems n pipe s = do
  objectNodes <- run pipe $ getNodes possessionQ :: IO [Either EuskeraAppError Lexis]
  nameNodes <- run pipe $ getNodes anthroponymsQ :: IO [Either EuskeraAppError Lexis]
  let objectNamePairs = take n $ zip (cycle $ rights objectNodes) (cycle $ rights nameNodes)
  let (sents, newState) = runState (mapM (uncurry possessiveItem) (take n objectNamePairs)) s
  showItemsWithHint possessiveHint "possessive" (createSentence <$> sents)
  return newState

--------------------------------------------------------------------------------
-- Output
--------------------------------------------------------------------------------

-- | Show items, one at a time, wait for response and give feedback
showItems :: Tag -> [Constituent] -> IO ()
showItems = showItems' 1

showItems' :: Int -> Tag -> [Constituent] -> IO ()
showItems' _ _ [] = do putStrLn "--"
showItems' n t (s : ss) = do
  putStr divider
  putStr $ show n ++ "  "
  putStrLn $ fromRight "" $ (instantiate . getItem . addSimpleGaps t) s
  answer <- getLine
  putStrLn $ giveFeedback answer (fromRight "error" $ instantiate s)
  showItems' (n + 1) t ss

-- | Show items, one at a time, wait for response and give feedback.
-- Additionally, provide a hint about the gap that has to be filled
showItemsWithHint :: Hint -> Tag -> [Constituent] -> IO ()
showItemsWithHint = showItemsWithHint' 1

showItemsWithHint' :: Int -> Hint -> Tag -> [Constituent] -> IO ()
showItemsWithHint' _ _ _ [] = do putStrLn "--"
showItemsWithHint' n hint t (s : ss) = do
  putStr divider
  putStr $ show n ++ "  "
  let item = addSimpleGaps t s -- these three lines should be a function
  putStr $ fromRight "" $ (instantiate . getItem) item
  putStrLn $ (hint . getAnswer) item
  answer <- getLine
  putStrLn $ giveFeedback answer (fromRight "" $ instantiate s)
  showItemsWithHint' (n + 1) hint t ss

giveFeedback :: String -> String -> String
giveFeedback answer correct
  | answer == correct = "\n  Oso ondo! :D" ++ divider
  | otherwise = "\n  La respuesta correcta era: " ++ correct ++ divider

--------------------------------------------------------------------------------
-- Helpers
--------------------------------------------------------------------------------
divider :: String
divider = "\n----------------------\n"

listToPairs :: [a] -> [(a, a)]
listToPairs [] = []
listToPairs [a] = []
listToPairs (a : b : xs) = (a, b) : listToPairs xs

pronounOrName :: NF -> Lexis -> Lexis
pronounOrName nf lexis =
  case (person nf, number nf) of
    (Third, Singular) -> lexis -- we need object number
    _ -> Lexis.N Pronoun nf

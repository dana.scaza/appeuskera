module EuskeraAppError where

-- | The available error types that can be registered
data EuskeraAppError
  = InflectionError String
  | FeatureError String
  | EmptyString String
  | ConversionError String
  | Unimplemented
  deriving (Eq, Show)

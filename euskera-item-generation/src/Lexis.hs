module Lexis
  ( Lexis (..),
    module Lexis.Noun,
    module Lexis.Verb,
    module Lexis.Adjective,
    module Lexis.Adverb,
    getLFeatures,
    modifyFeatures,
    isAnthroponym,
    getVerbFromLexis,
    modifyDeterminer,
    modifyCase,
    modifyPossessive,
    getNounNumber,
  )
where

import Features
import Lexis.Adjective
import Lexis.Adverb
import Lexis.Noun
import Lexis.Verb

data Lexis
  = N Nominal NF
  | V Verb VF
  | Adj Adjective NF -- we will need to extend features to include comparison in the future
  | Adv Adverb AdvF
  deriving (Eq, Show)

-- | Produce Lexis' features wrapped
getLFeatures :: Lexis -> FeatureWrapper
getLFeatures (N _ nf) = wrap nf
getLFeatures (V _ vf) = wrap vf
getLFeatures (Adj _ nf) = wrap nf
getLFeatures (Adv _ advf) = wrap advf

-- | Given an Lexis and a wrapped feature complex, create a new Lexis with
-- the given features
modifyFeatures :: Lexis -> FeatureWrapper -> Lexis
modifyFeatures (N w srcF) (WrapNF tgtF) = N w tgtF
modifyFeatures (V w srcF) (WrapVF tgtF) = V w tgtF
modifyFeatures (Adj w srcF) (WrapNF tgtF) = Adj w tgtF
modifyFeatures (Adv w srcF) (WrapAdvF tgtF) = Adv w tgtF
modifyFeatures _ _ = error "Incompatible word and feature types"

isAnthroponym :: Lexis -> Bool
isAnthroponym (N (Noun (Anthroponym _)) _) = True
isAnthroponym _ = False

getVerbFromLexis :: Lexis -> Verb
getVerbFromLexis (V verb vf) = verb
getVerbFromLexis _ = error "argument to getVerb was not a verb"

modifyDeterminer ::
  -- | Must be a noun
  Lexis ->
  Determiner ->
  Lexis
modifyDeterminer (N noun nf) d = N noun nf {determiner = d}

modifyCase ::
  Case ->
  -- | Must be a noun
  Lexis ->
  Lexis
modifyCase c (N noun nf) = N noun nf {grammCase = c}

modifyPossessive ::
  Possessive ->
  -- | Must be a noun
  Lexis ->
  Lexis
modifyPossessive p (N noun nf) = N noun nf {possessive = p}

getNounNumber :: Lexis -> Number
getNounNumber (N n nf) = number nf
getNounNumber n = error "incorrect lexis type"

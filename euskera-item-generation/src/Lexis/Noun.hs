module Lexis.Noun
  ( Nominal (..),
    Noun (..),
    getNoun,
  )
where

import Features

-- | The EmptyNoun class is used in cases where we want to omit a constituent.
data Nominal
  = Noun Noun
  | Pronoun
  | EmptyNoun
  deriving (Eq, Show)

-- | Noun subclasses
data Noun
  = Count String Number
  | Mass String
  | Proper String
  | Anthroponym String
  deriving (Eq, Show)

-- | Produces the noun's root
getNoun :: Noun -> String
getNoun (Count x _) = x
getNoun (Mass x) = x
getNoun (Proper x) = x
getNoun (Anthroponym x) = x

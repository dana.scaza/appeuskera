module Lexis.Adjective
  ( Adjective (..),
    getAdjective,
  )
where

data Adjective
  = Qualitative String
  | Relative String
  deriving (Eq, Show)

-- | Produces the adjective's root
getAdjective :: Adjective -> String
getAdjective (Qualitative x) = x
getAdjective (Relative x) = x

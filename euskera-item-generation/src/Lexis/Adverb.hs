module Lexis.Adverb (Adverb (..), getAdverb) where

newtype Adverb = A String deriving (Eq, Show)

-- | Produces the adverb's root
getAdverb :: Adverb -> String
getAdverb (A x) = x

module Lexis.Verb
  ( Verb (..),
    getVerb,
  )
where

-- | Verbs are classified according to the arguments they take
data Verb
  = Nor String
  | Copula String
  | NorNori String
  | NorNork String
  | NorNoriNork String
  deriving (Show, Eq)

-- | Produces the verb's root
getVerb :: Verb -> String
getVerb (Nor v) = v
getVerb (Copula v) = v
getVerb (NorNori v) = v
getVerb (NorNork v) = v
getVerb (NorNoriNork v) = v

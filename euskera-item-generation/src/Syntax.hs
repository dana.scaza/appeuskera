module Syntax
  ( module Syntax.Constituent,
    module Syntax.SentenceModels,
    module Syntax.NominalPhrases,
    module Syntax.ProtoSentence,
    module Syntax.VerbalPhrases,
  )
where

import Syntax.Constituent
import Syntax.NominalPhrases
import Syntax.ProtoSentence
import Syntax.SentenceModels
import Syntax.VerbalPhrases

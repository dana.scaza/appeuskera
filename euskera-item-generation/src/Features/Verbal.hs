module Features.Verbal
  ( VF (..),
    Aspect (..),
    Tense (..),
    Instantiation (..),
    fullVerb,
    lexicalVerb,
    auxOrTrinko,
    norConcord,
    noriConcord,
    norkConcord,
  )
where

import Features.Nominal

-- | Verbal features
data VF = VF
  { tense :: Tense,
    aspect :: Aspect,
    nor :: NF,
    nori :: NF,
    nork :: NF,
    instantiation :: Instantiation
  }
  deriving (Eq, Show)

-- TODO: nor, nori, nork could be only (Person,Number)

-- | Generate specified agreement on verb with given nominal features
norConcord, noriConcord, norkConcord :: NF -> VF -> VF
norConcord nf vf = vf {nor = nf}
noriConcord nf vf = vf {nori = nf}
norkConcord nf vf = vf {nork = nf}

-- | Convert VF into the describe Instantiation category
fullVerb, lexicalVerb, auxOrTrinko :: VF -> VF
fullVerb = changeVInstantiation Full
lexicalVerb = changeVInstantiation Main
auxOrTrinko = changeVInstantiation AuxOrTrinko

changeVInstantiation :: Instantiation -> VF -> VF
changeVInstantiation pos vf = vf {instantiation = pos}

-- | Aspectual features. Punctual is included only for irregular verbs.
data Aspect
  = Perfective
  | Imperfective
  | Future
  | Punctual
  deriving (Show, Eq)

-- Punctual is to be used in synthetic forms only

data Tense
  = Past
  | Present
  deriving (Show, Eq)

-- | This feature is not a linguistic category. It is necessary because we
--  might want to instantiate only the auxiliary or main parts of the verb.
data Instantiation
  = Full
  | Main
  | AuxOrTrinko
  deriving (Show, Eq)

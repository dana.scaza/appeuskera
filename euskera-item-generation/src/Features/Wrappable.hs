module Features.Wrappable where

import Features.Adverbial
import Features.Nominal
import Features.Sentential
import Features.Verbal

-- | This wrapper is used in cases when we need to be able to treat all
-- feature complexes uniformly
data FeatureWrapper
  = WrapNF NF
  | WrapVF VF
  | WrapSF SF
  | WrapAdvF AdvF
  deriving (Eq, Show)

class Wrappable a where
  wrap :: a -> FeatureWrapper

instance Wrappable NF where
  wrap (NF a b c d e f) = WrapNF (NF a b c d e f)

instance Wrappable VF where
  wrap (VF a b c d e f) = WrapVF (VF a b c d e f)

instance Wrappable SF where
  wrap (SF a) = WrapSF (SF a)

instance Wrappable AdvF where
  wrap BareAdv = WrapAdvF BareAdv

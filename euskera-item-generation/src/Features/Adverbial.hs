module Features.Adverbial (Distance (..), AdvF (..)) where

-- | Adverbial features. Not developed at the moment.
data AdvF = BareAdv deriving (Eq, Show)

-- | Distance degrees
data Distance
  = Close
  | Medium
  | Far
  deriving (Eq, Show)

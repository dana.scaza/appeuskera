module Features.Nominal
  ( NF (..),
    copyNF,
    personNumber,
    Person (..),
    Number (..),
    Case (..),
    Determiner (..),
    Possessive (..),
  )
where

import Features.Adverbial (Distance)

-- | Nominal feature complex used
data NF = NF
  { person :: Person,
    number :: Number,
    grammCase :: Case,
    possessive :: Possessive,
    determiner :: Determiner,
    coord :: Bool
  }
  deriving (Show, Eq)

-- | Copy Person and Number from source NF to target NF
copyNF :: NF -> NF -> NF
copyNF srcF tgtF =
  tgtF
    { person = person srcF,
      number = number srcF
    }

-- | Extract Person and Number from NF
personNumber :: NF -> (Person, Number)
personNumber nf = (person nf, number nf)

--------------------------------------------------------------------------------
-- Basic nominal features
--------------------------------------------------------------------------------

data Person
  = First
  | Second
  | Third
  deriving (Show, Eq)

-- | The feature Zero is necessary to have mempty on monoids.
data Number
  = Singular
  | Plural
  | Zero
  deriving (Show, Eq)

-- | Cases are not complete. They will be extended as the application grows
data Case
  = Absolutive
  | Ergative
  | Dative
  | Genitive Person Number (Maybe Case)
  deriving (Show, Eq)

-- | A noun can have definite determination (default) or indefinite.
--   Additionally, they can be determined through Demonstratives (with a distance degree)
data Determiner
  = Dem Distance
  | Definite
  | Indefinite
  deriving
    (Show, Eq)

-- Demonstrative could be a subclass of Definite

-- | A noun can either be modified by a possessive or not
data Possessive
  = Poss Person Number
  | NoPoss
  deriving (Show, Eq)

--------------------------------------------------------------------------------
-- Monoids for Person and Number
--------------------------------------------------------------------------------

instance Semigroup Person where
  First <> _ = First
  _ <> First = First
  Second <> _ = Second
  _ <> Second = Second
  _ <> _ = Third

instance Monoid Person where
  mempty = Third

instance Semigroup Number where
  Zero <> Zero = Zero
  Zero <> Singular = Singular
  Singular <> Zero = Singular
  _ <> _ = Plural

instance Monoid Number where
  mempty = Zero

module Features.Sentential
  ( SF (..),
    SentenceType (..),
  )
where

-- | Sentence features
data SF = SF SentenceType deriving (Eq, Show)

-- | At the moment, a sentence can be of only two types.
data SentenceType
  = Declarative
  | YesNoInterrogative
  deriving (Eq, Show)

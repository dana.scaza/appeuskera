# Euskera App

## Setting up the Neo4J database

You can install [Neo4J](https://neo4j.com/) and the [APOC library](https://neo4j.com/developer/neo4j-apoc/) and populate database with the scripts found in `db/cypher_scripts`.
Be sure to set up the password to "euskera".

Alternatively, you can install [docker](https://www.docker.com/) and run Neo4J
from a container. The script found in `db/set_up_db.sh` will set up a Neo4J database for you.

With the database running, you can run `populate_db.sh` to populate the database with
data. Be sure to run it from the `db` folder.

You will be able to inspect the database through a web browser at: `localhost:7474`,
signing in with the username 'neo4j' and the password 'euskera'.

Use `MATCH (n) RETURN n` to inspect all nodes.


## Building with Stack

The project has been built using [Stack](https://docs.haskellstack.org/en/stable/GUIDE/), and therefore it is recommended to use Stack to build it. You can find installation instructions [here](https://docs.haskellstack.org/en/stable/GUIDE/).

With stack installed, simply run the following commands:

```
cd /path/to/euskera-item-generation
stack build
stack exec euskera-item-generation-exe
```
The `stack build` command will take a long time the first time it is run, since it
will download all the necessary dependencies.

At the moment, the program shows a simple command-line interface that will allow you
to select among 5 exercises.



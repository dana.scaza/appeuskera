MATCH (j:Occupation), (p:Person)
CREATE (abeslari1:Common:Noun {level:"A1",root:"abeslari"})
CREATE (abokatu2:Common:Noun {level:"A1",root:"abokatu"})
CREATE (aktore3:Common:Noun {level:"A1",root:"aktore"})
CREATE (albaitari4:Common:Noun {level:"A1",root:"albaitari"})
CREATE (argazkilari5:Common:Noun {level:"A1",root:"argazkilari"})
CREATE (arkitekto6:Common:Noun {level:"A1",root:"arkitekto"})
CREATE (arotz7:Common:Noun {level:"A1",root:"arotz"})
CREATE (arrain8:Common:Noun {level:"A1",root:"arrain-saltzaile"})
CREATE (arrantzale9:Common:Noun {level:"A1",root:"arrantzale"})
CREATE (artista10:Common:Noun {level:"A1",root:"artista"})
CREATE (astronauta11:Common:Noun {level:"A1",root:"astronauta"})
CREATE (bankari12:Common:Noun {level:"A1",root:"bankari"})
CREATE (baserritar13:Common:Noun {level:"A1",root:"baserritar"})
CREATE (biologo14:Common:Noun {level:"A1",root:"biologo"})
CREATE (botikari15:Common:Noun {level:"A1",root:"botikari"})
CREATE (bitxigile16:Common:Noun {level:"A1",root:"bitxigile"})
CREATE (bulegari17:Common:Noun {level:"A1",root:"bulegari"})
CREATE (dantza18:Common:Noun {level:"A1",root:"dantza-irakasle"})
CREATE (dantzari19:Common:Noun {level:"A1",root:"dantzari"})
CREATE (dendari20:Common:Noun {level:"A1",root:"dendari"})
CREATE (diseinatzaile21:Common:Noun {level:"A1",root:"diseinatzaile"})
CREATE (diseinatzaile22:Common:Noun {level:"A1",root:"diseinatzaile grafiko"})
CREATE (elektrikari23:Common:Noun {level:"A1",root:"elektrikari"})
CREATE (enpresari24:Common:Noun {level:"A1",root:"enpresari"})
CREATE (epaile25:Common:Noun {level:"A1",root:"epaile"})
CREATE (erizain26:Common:Noun {level:"A1",root:"erizain"})
CREATE (ertzain27:Common:Noun {level:"A1",root:"ertzain"})
CREATE (futbol28:Common:Noun {level:"A1",root:"futbol-jokalari"})
CREATE (garbitzaile29:Common:Noun {level:"A1",root:"garbitzaile"})
CREATE (gidari30:Common:Noun {level:"A1",root:"gidari"})
CREATE (idazkari31:Common:Noun {level:"A1",root:"idazkari"})
CREATE (idazle32:Common:Noun {level:"A1",root:"idazle"})
CREATE (igeltsero33:Common:Noun {level:"A1",root:"igeltsero"})
CREATE (ile34:Common:Noun {level:"A1",root:"ile-apaintzaile"})
CREATE (informatikari35:Common:Noun {level:"A1",root:"informatikari"})
CREATE (ingeniari36:Common:Noun {level:"A1",root:"ingeniari"})
CREATE (irakasle37:Common:Noun {level:"A1",root:"irakasle"})
CREATE (ikasle38:Common:Noun {level:"A1",root:"ikasle"})
CREATE (iturgin39:Common:Noun {level:"A1",root:"iturgin"})
CREATE (itzultzaile40:Common:Noun {level:"A1",root:"itzultzaile"})
CREATE (harakin41:Common:Noun {level:"A1",root:"harakin"})
CREATE (kamiolari42:Common:Noun {level:"A1",root:"kamiolari"})
CREATE (kale43:Common:Noun {level:"A1",root:"kale-garbitzaile"})
CREATE (kantari44:Common:Noun {level:"A1",root:"kantari"})
CREATE (kazetari45:Common:Noun {level:"A1",root:"kazetari"})
CREATE (kirolari46:Common:Noun {level:"A1",root:"kirolari"})
CREATE (koordinatzaile47:Common:Noun {level:"A1",root:"koordinatzaile"})
CREATE (lapur48:Common:Noun {level:"A1",root:"lapur"})
CREATE (lorezain49:Common:Noun {level:"A1",root:"lorezain"})
CREATE (margolari50:Common:Noun {level:"A1",root:"margolari"})
CREATE (meatzari51:Common:Noun {level:"A1",root:"meatzari"})
CREATE (mediku52:Common:Noun {level:"A1",root:"mediku"})
CREATE (mekanikari53:Common:Noun {level:"A1",root:"mekanikari"})
CREATE (moda54:Common:Noun {level:"A1",root:"moda-diseinatzaile"})
CREATE (modelo55:Common:Noun {level:"A1",root:"modelo"})
CREATE (musika56:Common:Noun {level:"A1",root:"musika-irakasle"})
CREATE (musikari57:Common:Noun {level:"A1",root:"musikari"})
CREATE (nekazari58:Common:Noun {level:"A1",root:"nekazari"})
CREATE (okin59:Common:Noun {level:"A1",root:"okin"})
CREATE (pailazo60:Common:Noun {level:"A1",root:"pailazo"})
CREATE (pilotu61:Common:Noun {level:"A1",root:"pilotu"})
CREATE (politikari62:Common:Noun {level:"A1",root:"politikari"})
CREATE (postari63:Common:Noun {level:"A1",root:"postari"})
CREATE (psikologo64:Common:Noun {level:"A1",root:"psikologo"})
CREATE (programatzaile65:Common:Noun {level:"A1",root:"programatzaile"})
CREATE (saltzaile66:Common:Noun {level:"A1",root:"saltzaile"})
CREATE (suhiltzaile67:Common:Noun {level:"A1",root:"suhiltzaile"})
CREATE (sukaldari68:Common:Noun {level:"A1",root:"sukaldari"})
CREATE (taxi69:Common:Noun {level:"A1",root:"taxi-gidari"})
CREATE (udaltzain70:Common:Noun {level:"A1",root:"udaltzain"})
CREATE (zapatari71:Common:Noun {level:"A1",root:"zapatari"})
CREATE (zerbitzari72:Common:Noun {level:"A1",root:"zerbitzari"})
CREATE (zientzialari73:Common:Noun {level:"A1",root:"zientzialari"})
CREATE (zinema74:Common:Noun {level:"A1",root:"zinema-zuzendari"})
CREATE (zorro75:Common:Noun {level:"A1",root:"zorro-lapur"})
CREATE (abeslari1)-[:INTENSIVE]->(j)
CREATE (abeslari1)-[:IDENTITY]->(j)

CREATE (abokatu2)-[:INTENSIVE]->(j)
CREATE (abokatu2)-[:IDENTITY]->(j)

CREATE (aktore3)-[:INTENSIVE]->(j)
CREATE (aktore3)-[:IDENTITY]->(j)

CREATE (albaitari4)-[:INTENSIVE]->(j)
CREATE (albaitari4)-[:IDENTITY]->(j)

CREATE (argazkilari5)-[:INTENSIVE]->(j)
CREATE (argazkilari5)-[:IDENTITY]->(j)

CREATE (arkitekto6)-[:INTENSIVE]->(j)
CREATE (arkitekto6)-[:IDENTITY]->(j)

CREATE (arotz7)-[:INTENSIVE]->(j)
CREATE (arotz7)-[:IDENTITY]->(j)

CREATE (arrain8)-[:INTENSIVE]->(j)
CREATE (arrain8)-[:IDENTITY]->(j)

CREATE (arrantzale9)-[:INTENSIVE]->(j)
CREATE (arrantzale9)-[:IDENTITY]->(j)

CREATE (artista10)-[:INTENSIVE]->(j)
CREATE (artista10)-[:IDENTITY]->(j)

CREATE (astronauta11)-[:INTENSIVE]->(j)
CREATE (astronauta11)-[:IDENTITY]->(j)

CREATE (bankari12)-[:INTENSIVE]->(j)
CREATE (bankari12)-[:IDENTITY]->(j)

CREATE (baserritar13)-[:INTENSIVE]->(j)
CREATE (baserritar13)-[:IDENTITY]->(j)

CREATE (biologo14)-[:INTENSIVE]->(j)
CREATE (biologo14)-[:IDENTITY]->(j)

CREATE (botikari15)-[:INTENSIVE]->(j)
CREATE (botikari15)-[:IDENTITY]->(j)

CREATE (bitxigile16)-[:INTENSIVE]->(j)
CREATE (bitxigile16)-[:IDENTITY]->(j)

CREATE (bulegari17)-[:INTENSIVE]->(j)
CREATE (bulegari17)-[:IDENTITY]->(j)

CREATE (dantza18)-[:INTENSIVE]->(j)
CREATE (dantza18)-[:IDENTITY]->(j)

CREATE (dantzari19)-[:INTENSIVE]->(j)
CREATE (dantzari19)-[:IDENTITY]->(j)

CREATE (dendari20)-[:INTENSIVE]->(j)
CREATE (dendari20)-[:IDENTITY]->(j)

CREATE (diseinatzaile21)-[:INTENSIVE]->(j)
CREATE (diseinatzaile21)-[:IDENTITY]->(j)

CREATE (diseinatzaile22)-[:INTENSIVE]->(j)
CREATE (diseinatzaile22)-[:IDENTITY]->(j)

CREATE (elektrikari23)-[:INTENSIVE]->(j)
CREATE (elektrikari23)-[:IDENTITY]->(j)

CREATE (enpresari24)-[:INTENSIVE]->(j)
CREATE (enpresari24)-[:IDENTITY]->(j)

CREATE (epaile25)-[:INTENSIVE]->(j)
CREATE (epaile25)-[:IDENTITY]->(j)

CREATE (erizain26)-[:INTENSIVE]->(j)
CREATE (erizain26)-[:IDENTITY]->(j)

CREATE (ertzain27)-[:INTENSIVE]->(j)
CREATE (ertzain27)-[:IDENTITY]->(j)

CREATE (futbol28)-[:INTENSIVE]->(j)
CREATE (futbol28)-[:IDENTITY]->(j)

CREATE (garbitzaile29)-[:INTENSIVE]->(j)
CREATE (garbitzaile29)-[:IDENTITY]->(j)

CREATE (gidari30)-[:INTENSIVE]->(j)
CREATE (gidari30)-[:IDENTITY]->(j)

CREATE (idazkari31)-[:INTENSIVE]->(j)
CREATE (idazkari31)-[:IDENTITY]->(j)

CREATE (idazle32)-[:INTENSIVE]->(j)
CREATE (idazle32)-[:IDENTITY]->(j)

CREATE (igeltsero33)-[:INTENSIVE]->(j)
CREATE (igeltsero33)-[:IDENTITY]->(j)

CREATE (ile34)-[:INTENSIVE]->(j)
CREATE (ile34)-[:IDENTITY]->(j)

CREATE (informatikari35)-[:INTENSIVE]->(j)
CREATE (informatikari35)-[:IDENTITY]->(j)

CREATE (ingeniari36)-[:INTENSIVE]->(j)
CREATE (ingeniari36)-[:IDENTITY]->(j)

CREATE (irakasle37)-[:INTENSIVE]->(j)
CREATE (irakasle37)-[:IDENTITY]->(j)

CREATE (ikasle38)-[:INTENSIVE]->(j)
CREATE (ikasle38)-[:IDENTITY]->(j)

CREATE (iturgin39)-[:INTENSIVE]->(j)
CREATE (iturgin39)-[:IDENTITY]->(j)

CREATE (itzultzaile40)-[:INTENSIVE]->(j)
CREATE (itzultzaile40)-[:IDENTITY]->(j)

CREATE (harakin41)-[:INTENSIVE]->(j)
CREATE (harakin41)-[:IDENTITY]->(j)

CREATE (kamiolari42)-[:INTENSIVE]->(j)
CREATE (kamiolari42)-[:IDENTITY]->(j)

CREATE (kale43)-[:INTENSIVE]->(j)
CREATE (kale43)-[:IDENTITY]->(j)

CREATE (kantari44)-[:INTENSIVE]->(j)
CREATE (kantari44)-[:IDENTITY]->(j)

CREATE (kazetari45)-[:INTENSIVE]->(j)
CREATE (kazetari45)-[:IDENTITY]->(j)

CREATE (kirolari46)-[:INTENSIVE]->(j)
CREATE (kirolari46)-[:IDENTITY]->(j)

CREATE (koordinatzaile47)-[:INTENSIVE]->(j)
CREATE (koordinatzaile47)-[:IDENTITY]->(j)

CREATE (lapur48)-[:INTENSIVE]->(j)
CREATE (lapur48)-[:IDENTITY]->(j)

CREATE (lorezain49)-[:INTENSIVE]->(j)
CREATE (lorezain49)-[:IDENTITY]->(j)

CREATE (margolari50)-[:INTENSIVE]->(j)
CREATE (margolari50)-[:IDENTITY]->(j)

CREATE (meatzari51)-[:INTENSIVE]->(j)
CREATE (meatzari51)-[:IDENTITY]->(j)

CREATE (mediku52)-[:INTENSIVE]->(j)
CREATE (mediku52)-[:IDENTITY]->(j)

CREATE (mekanikari53)-[:INTENSIVE]->(j)
CREATE (mekanikari53)-[:IDENTITY]->(j)

CREATE (moda54)-[:INTENSIVE]->(j)
CREATE (moda54)-[:IDENTITY]->(j)

CREATE (modelo55)-[:INTENSIVE]->(j)
CREATE (modelo55)-[:IDENTITY]->(j)

CREATE (musika56)-[:INTENSIVE]->(j)
CREATE (musika56)-[:IDENTITY]->(j)

CREATE (musikari57)-[:INTENSIVE]->(j)
CREATE (musikari57)-[:IDENTITY]->(j)

CREATE (nekazari58)-[:INTENSIVE]->(j)
CREATE (nekazari58)-[:IDENTITY]->(j)

CREATE (okin59)-[:INTENSIVE]->(j)
CREATE (okin59)-[:IDENTITY]->(j)

CREATE (pailazo60)-[:INTENSIVE]->(j)
CREATE (pailazo60)-[:IDENTITY]->(j)

CREATE (pilotu61)-[:INTENSIVE]->(j)
CREATE (pilotu61)-[:IDENTITY]->(j)

CREATE (politikari62)-[:INTENSIVE]->(j)
CREATE (politikari62)-[:IDENTITY]->(j)

CREATE (postari63)-[:INTENSIVE]->(j)
CREATE (postari63)-[:IDENTITY]->(j)

CREATE (psikologo64)-[:INTENSIVE]->(j)
CREATE (psikologo64)-[:IDENTITY]->(j)

CREATE (programatzaile65)-[:INTENSIVE]->(j)
CREATE (programatzaile65)-[:IDENTITY]->(j)

CREATE (saltzaile66)-[:INTENSIVE]->(j)
CREATE (saltzaile66)-[:IDENTITY]->(j)

CREATE (suhiltzaile67)-[:INTENSIVE]->(j)
CREATE (suhiltzaile67)-[:IDENTITY]->(j)

CREATE (sukaldari68)-[:INTENSIVE]->(j)
CREATE (sukaldari68)-[:IDENTITY]->(j)

CREATE (taxi69)-[:INTENSIVE]->(j)
CREATE (taxi69)-[:IDENTITY]->(j)

CREATE (udaltzain70)-[:INTENSIVE]->(j)
CREATE (udaltzain70)-[:IDENTITY]->(j)

CREATE (zapatari71)-[:INTENSIVE]->(j)
CREATE (zapatari71)-[:IDENTITY]->(j)

CREATE (zerbitzari72)-[:INTENSIVE]->(j)
CREATE (zerbitzari72)-[:IDENTITY]->(j)

CREATE (zientzialari73)-[:INTENSIVE]->(j)
CREATE (zientzialari73)-[:IDENTITY]->(j)

CREATE (zinema74)-[:INTENSIVE]->(j)
CREATE (zinema74)-[:IDENTITY]->(j)

CREATE (zorro75)-[:INTENSIVE]->(j)
CREATE (zorro75)-[:IDENTITY]->(j)

CREATE (p)-[:INTENSIVE]->(abeslari1)
CREATE (p)-[:ASCRIPTION]->(abeslari1)
CREATE (p)-[:CLASS_ASCRIPTION]->(abeslari1)

CREATE (p)-[:INTENSIVE]->(abokatu2)
CREATE (p)-[:ASCRIPTION]->(abokatu2)
CREATE (p)-[:CLASS_ASCRIPTION]->(abokatu2)

CREATE (p)-[:INTENSIVE]->(aktore3)
CREATE (p)-[:ASCRIPTION]->(aktore3)
CREATE (p)-[:CLASS_ASCRIPTION]->(aktore3)

CREATE (p)-[:INTENSIVE]->(albaitari4)
CREATE (p)-[:ASCRIPTION]->(albaitari4)
CREATE (p)-[:CLASS_ASCRIPTION]->(albaitari4)

CREATE (p)-[:INTENSIVE]->(argazkilari5)
CREATE (p)-[:ASCRIPTION]->(argazkilari5)
CREATE (p)-[:CLASS_ASCRIPTION]->(argazkilari5)

CREATE (p)-[:INTENSIVE]->(arkitekto6)
CREATE (p)-[:ASCRIPTION]->(arkitekto6)
CREATE (p)-[:CLASS_ASCRIPTION]->(arkitekto6)

CREATE (p)-[:INTENSIVE]->(arotz7)
CREATE (p)-[:ASCRIPTION]->(arotz7)
CREATE (p)-[:CLASS_ASCRIPTION]->(arotz7)

CREATE (p)-[:INTENSIVE]->(arrain8)
CREATE (p)-[:ASCRIPTION]->(arrain8)
CREATE (p)-[:CLASS_ASCRIPTION]->(arrain8)

CREATE (p)-[:INTENSIVE]->(arrantzale9)
CREATE (p)-[:ASCRIPTION]->(arrantzale9)
CREATE (p)-[:CLASS_ASCRIPTION]->(arrantzale9)

CREATE (p)-[:INTENSIVE]->(artista10)
CREATE (p)-[:ASCRIPTION]->(artista10)
CREATE (p)-[:CLASS_ASCRIPTION]->(artista10)

CREATE (p)-[:INTENSIVE]->(astronauta11)
CREATE (p)-[:ASCRIPTION]->(astronauta11)
CREATE (p)-[:CLASS_ASCRIPTION]->(astronauta11)

CREATE (p)-[:INTENSIVE]->(bankari12)
CREATE (p)-[:ASCRIPTION]->(bankari12)
CREATE (p)-[:CLASS_ASCRIPTION]->(bankari12)

CREATE (p)-[:INTENSIVE]->(baserritar13)
CREATE (p)-[:ASCRIPTION]->(baserritar13)
CREATE (p)-[:CLASS_ASCRIPTION]->(baserritar13)

CREATE (p)-[:INTENSIVE]->(biologo14)
CREATE (p)-[:ASCRIPTION]->(biologo14)
CREATE (p)-[:CLASS_ASCRIPTION]->(biologo14)

CREATE (p)-[:INTENSIVE]->(botikari15)
CREATE (p)-[:ASCRIPTION]->(botikari15)
CREATE (p)-[:CLASS_ASCRIPTION]->(botikari15)

CREATE (p)-[:INTENSIVE]->(bitxigile16)
CREATE (p)-[:ASCRIPTION]->(bitxigile16)
CREATE (p)-[:CLASS_ASCRIPTION]->(bitxigile16)

CREATE (p)-[:INTENSIVE]->(bulegari17)
CREATE (p)-[:ASCRIPTION]->(bulegari17)
CREATE (p)-[:CLASS_ASCRIPTION]->(bulegari17)

CREATE (p)-[:INTENSIVE]->(dantza18)
CREATE (p)-[:ASCRIPTION]->(dantza18)
CREATE (p)-[:CLASS_ASCRIPTION]->(dantza18)

CREATE (p)-[:INTENSIVE]->(dantzari19)
CREATE (p)-[:ASCRIPTION]->(dantzari19)
CREATE (p)-[:CLASS_ASCRIPTION]->(dantzari19)

CREATE (p)-[:INTENSIVE]->(dendari20)
CREATE (p)-[:ASCRIPTION]->(dendari20)
CREATE (p)-[:CLASS_ASCRIPTION]->(dendari20)

CREATE (p)-[:INTENSIVE]->(diseinatzaile21)
CREATE (p)-[:ASCRIPTION]->(diseinatzaile21)
CREATE (p)-[:CLASS_ASCRIPTION]->(diseinatzaile21)

CREATE (p)-[:INTENSIVE]->(diseinatzaile22)
CREATE (p)-[:ASCRIPTION]->(diseinatzaile22)
CREATE (p)-[:CLASS_ASCRIPTION]->(diseinatzaile22)

CREATE (p)-[:INTENSIVE]->(elektrikari23)
CREATE (p)-[:ASCRIPTION]->(elektrikari23)
CREATE (p)-[:CLASS_ASCRIPTION]->(elektrikari23)

CREATE (p)-[:INTENSIVE]->(enpresari24)
CREATE (p)-[:ASCRIPTION]->(enpresari24)
CREATE (p)-[:CLASS_ASCRIPTION]->(enpresari24)

CREATE (p)-[:INTENSIVE]->(epaile25)
CREATE (p)-[:ASCRIPTION]->(epaile25)
CREATE (p)-[:CLASS_ASCRIPTION]->(epaile25)

CREATE (p)-[:INTENSIVE]->(erizain26)
CREATE (p)-[:ASCRIPTION]->(erizain26)
CREATE (p)-[:CLASS_ASCRIPTION]->(erizain26)

CREATE (p)-[:INTENSIVE]->(ertzain27)
CREATE (p)-[:ASCRIPTION]->(ertzain27)
CREATE (p)-[:CLASS_ASCRIPTION]->(ertzain27)

CREATE (p)-[:INTENSIVE]->(futbol28)
CREATE (p)-[:ASCRIPTION]->(futbol28)
CREATE (p)-[:CLASS_ASCRIPTION]->(futbol28)

CREATE (p)-[:INTENSIVE]->(garbitzaile29)
CREATE (p)-[:ASCRIPTION]->(garbitzaile29)
CREATE (p)-[:CLASS_ASCRIPTION]->(garbitzaile29)

CREATE (p)-[:INTENSIVE]->(gidari30)
CREATE (p)-[:ASCRIPTION]->(gidari30)
CREATE (p)-[:CLASS_ASCRIPTION]->(gidari30)

CREATE (p)-[:INTENSIVE]->(idazkari31)
CREATE (p)-[:ASCRIPTION]->(idazkari31)
CREATE (p)-[:CLASS_ASCRIPTION]->(idazkari31)

CREATE (p)-[:INTENSIVE]->(idazle32)
CREATE (p)-[:ASCRIPTION]->(idazle32)
CREATE (p)-[:CLASS_ASCRIPTION]->(idazle32)

CREATE (p)-[:INTENSIVE]->(igeltsero33)
CREATE (p)-[:ASCRIPTION]->(igeltsero33)
CREATE (p)-[:CLASS_ASCRIPTION]->(igeltsero33)

CREATE (p)-[:INTENSIVE]->(ile34)
CREATE (p)-[:ASCRIPTION]->(ile34)
CREATE (p)-[:CLASS_ASCRIPTION]->(ile34)

CREATE (p)-[:INTENSIVE]->(informatikari35)
CREATE (p)-[:ASCRIPTION]->(informatikari35)
CREATE (p)-[:CLASS_ASCRIPTION]->(informatikari35)

CREATE (p)-[:INTENSIVE]->(ingeniari36)
CREATE (p)-[:ASCRIPTION]->(ingeniari36)
CREATE (p)-[:CLASS_ASCRIPTION]->(ingeniari36)

CREATE (p)-[:INTENSIVE]->(irakasle37)
CREATE (p)-[:ASCRIPTION]->(irakasle37)
CREATE (p)-[:CLASS_ASCRIPTION]->(irakasle37)

CREATE (p)-[:INTENSIVE]->(ikasle38)
CREATE (p)-[:ASCRIPTION]->(ikasle38)
CREATE (p)-[:CLASS_ASCRIPTION]->(ikasle38)

CREATE (p)-[:INTENSIVE]->(iturgin39)
CREATE (p)-[:ASCRIPTION]->(iturgin39)
CREATE (p)-[:CLASS_ASCRIPTION]->(iturgin39)

CREATE (p)-[:INTENSIVE]->(itzultzaile40)
CREATE (p)-[:ASCRIPTION]->(itzultzaile40)
CREATE (p)-[:CLASS_ASCRIPTION]->(itzultzaile40)

CREATE (p)-[:INTENSIVE]->(harakin41)
CREATE (p)-[:ASCRIPTION]->(harakin41)
CREATE (p)-[:CLASS_ASCRIPTION]->(harakin41)

CREATE (p)-[:INTENSIVE]->(kamiolari42)
CREATE (p)-[:ASCRIPTION]->(kamiolari42)
CREATE (p)-[:CLASS_ASCRIPTION]->(kamiolari42)

CREATE (p)-[:INTENSIVE]->(kale43)
CREATE (p)-[:ASCRIPTION]->(kale43)
CREATE (p)-[:CLASS_ASCRIPTION]->(kale43)

CREATE (p)-[:INTENSIVE]->(kantari44)
CREATE (p)-[:ASCRIPTION]->(kantari44)
CREATE (p)-[:CLASS_ASCRIPTION]->(kantari44)

CREATE (p)-[:INTENSIVE]->(kazetari45)
CREATE (p)-[:ASCRIPTION]->(kazetari45)
CREATE (p)-[:CLASS_ASCRIPTION]->(kazetari45)

CREATE (p)-[:INTENSIVE]->(kirolari46)
CREATE (p)-[:ASCRIPTION]->(kirolari46)
CREATE (p)-[:CLASS_ASCRIPTION]->(kirolari46)

CREATE (p)-[:INTENSIVE]->(koordinatzaile47)
CREATE (p)-[:ASCRIPTION]->(koordinatzaile47)
CREATE (p)-[:CLASS_ASCRIPTION]->(koordinatzaile47)

CREATE (p)-[:INTENSIVE]->(lapur48)
CREATE (p)-[:ASCRIPTION]->(lapur48)
CREATE (p)-[:CLASS_ASCRIPTION]->(lapur48)

CREATE (p)-[:INTENSIVE]->(lorezain49)
CREATE (p)-[:ASCRIPTION]->(lorezain49)
CREATE (p)-[:CLASS_ASCRIPTION]->(lorezain49)

CREATE (p)-[:INTENSIVE]->(margolari50)
CREATE (p)-[:ASCRIPTION]->(margolari50)
CREATE (p)-[:CLASS_ASCRIPTION]->(margolari50)

CREATE (p)-[:INTENSIVE]->(meatzari51)
CREATE (p)-[:ASCRIPTION]->(meatzari51)
CREATE (p)-[:CLASS_ASCRIPTION]->(meatzari51)

CREATE (p)-[:INTENSIVE]->(mediku52)
CREATE (p)-[:ASCRIPTION]->(mediku52)
CREATE (p)-[:CLASS_ASCRIPTION]->(mediku52)

CREATE (p)-[:INTENSIVE]->(mekanikari53)
CREATE (p)-[:ASCRIPTION]->(mekanikari53)
CREATE (p)-[:CLASS_ASCRIPTION]->(mekanikari53)

CREATE (p)-[:INTENSIVE]->(moda54)
CREATE (p)-[:ASCRIPTION]->(moda54)
CREATE (p)-[:CLASS_ASCRIPTION]->(moda54)

CREATE (p)-[:INTENSIVE]->(modelo55)
CREATE (p)-[:ASCRIPTION]->(modelo55)
CREATE (p)-[:CLASS_ASCRIPTION]->(modelo55)

CREATE (p)-[:INTENSIVE]->(musika56)
CREATE (p)-[:ASCRIPTION]->(musika56)
CREATE (p)-[:CLASS_ASCRIPTION]->(musika56)

CREATE (p)-[:INTENSIVE]->(musikari57)
CREATE (p)-[:ASCRIPTION]->(musikari57)
CREATE (p)-[:CLASS_ASCRIPTION]->(musikari57)

CREATE (p)-[:INTENSIVE]->(nekazari58)
CREATE (p)-[:ASCRIPTION]->(nekazari58)
CREATE (p)-[:CLASS_ASCRIPTION]->(nekazari58)

CREATE (p)-[:INTENSIVE]->(okin59)
CREATE (p)-[:ASCRIPTION]->(okin59)
CREATE (p)-[:CLASS_ASCRIPTION]->(okin59)

CREATE (p)-[:INTENSIVE]->(pailazo60)
CREATE (p)-[:ASCRIPTION]->(pailazo60)
CREATE (p)-[:CLASS_ASCRIPTION]->(pailazo60)

CREATE (p)-[:INTENSIVE]->(pilotu61)
CREATE (p)-[:ASCRIPTION]->(pilotu61)
CREATE (p)-[:CLASS_ASCRIPTION]->(pilotu61)

CREATE (p)-[:INTENSIVE]->(politikari62)
CREATE (p)-[:ASCRIPTION]->(politikari62)
CREATE (p)-[:CLASS_ASCRIPTION]->(politikari62)

CREATE (p)-[:INTENSIVE]->(postari63)
CREATE (p)-[:ASCRIPTION]->(postari63)
CREATE (p)-[:CLASS_ASCRIPTION]->(postari63)

CREATE (p)-[:INTENSIVE]->(psikologo64)
CREATE (p)-[:ASCRIPTION]->(psikologo64)
CREATE (p)-[:CLASS_ASCRIPTION]->(psikologo64)

CREATE (p)-[:INTENSIVE]->(programatzaile65)
CREATE (p)-[:ASCRIPTION]->(programatzaile65)
CREATE (p)-[:CLASS_ASCRIPTION]->(programatzaile65)

CREATE (p)-[:INTENSIVE]->(saltzaile66)
CREATE (p)-[:ASCRIPTION]->(saltzaile66)
CREATE (p)-[:CLASS_ASCRIPTION]->(saltzaile66)

CREATE (p)-[:INTENSIVE]->(suhiltzaile67)
CREATE (p)-[:ASCRIPTION]->(suhiltzaile67)
CREATE (p)-[:CLASS_ASCRIPTION]->(suhiltzaile67)

CREATE (p)-[:INTENSIVE]->(sukaldari68)
CREATE (p)-[:ASCRIPTION]->(sukaldari68)
CREATE (p)-[:CLASS_ASCRIPTION]->(sukaldari68)

CREATE (p)-[:INTENSIVE]->(taxi69)
CREATE (p)-[:ASCRIPTION]->(taxi69)
CREATE (p)-[:CLASS_ASCRIPTION]->(taxi69)

CREATE (p)-[:INTENSIVE]->(udaltzain70)
CREATE (p)-[:ASCRIPTION]->(udaltzain70)
CREATE (p)-[:CLASS_ASCRIPTION]->(udaltzain70)

CREATE (p)-[:INTENSIVE]->(zapatari71)
CREATE (p)-[:ASCRIPTION]->(zapatari71)
CREATE (p)-[:CLASS_ASCRIPTION]->(zapatari71)

CREATE (p)-[:INTENSIVE]->(zerbitzari72)
CREATE (p)-[:ASCRIPTION]->(zerbitzari72)
CREATE (p)-[:CLASS_ASCRIPTION]->(zerbitzari72)

CREATE (p)-[:INTENSIVE]->(zientzialari73)
CREATE (p)-[:ASCRIPTION]->(zientzialari73)
CREATE (p)-[:CLASS_ASCRIPTION]->(zientzialari73)

CREATE (p)-[:INTENSIVE]->(zinema74)
CREATE (p)-[:ASCRIPTION]->(zinema74)
CREATE (p)-[:CLASS_ASCRIPTION]->(zinema74)

CREATE (p)-[:INTENSIVE]->(zorro75)
CREATE (p)-[:ASCRIPTION]->(zorro75)
CREATE (p)-[:CLASS_ASCRIPTION]->(zorro75)


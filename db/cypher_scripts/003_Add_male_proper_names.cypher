MATCH (m:Male)-[:IDENTITY]->(:Person)
CREATE (Amets:Anthroponym:Noun {level:"A1",root:"Amets"})
CREATE (Aritz:Anthroponym:Noun {level:"A1",root:"Aritz"})
CREATE (Arkaitz:Anthroponym:Noun {level:"A1",root:"Arkaitz"})
CREATE (Eneko:Anthroponym:Noun {level:"A1",root:"Eneko"})
CREATE (Fernando:Anthroponym:Noun {level:"A1",root:"Fernando"})
CREATE (Ibai:Anthroponym:Noun {level:"A1",root:"Ibai"})
CREATE (Iker:Anthroponym:Noun {level:"A1",root:"Iker"})
CREATE (Iñigo:Anthroponym:Noun {level:"A1",root:"Iñigo"})
CREATE (Ixak:Anthroponym:Noun {level:"A1",root:"Ixak"})
CREATE (Jagoba:Anthroponym:Noun {level:"A1",root:"Jagoba"})
CREATE (Jon:Anthroponym:Noun {level:"A1",root:"Jon"})
CREATE (Joseba:Anthroponym:Noun {level:"A1",root:"Joseba"})
CREATE (Josu:Anthroponym:Noun {level:"A1",root:"Josu"})
CREATE (Karra:Anthroponym:Noun {level:"A1",root:"Karra"})
CREATE (Kemen:Anthroponym:Noun {level:"A1",root:"Kemen"})
CREATE (Oihan:Anthroponym:Noun {level:"A1",root:"Oihan"})
CREATE (Oiher:Anthroponym:Noun {level:"A1",root:"Oiher"})
CREATE (Amets)-[:INTENSIVE]->(m)
CREATE (Amets)-[:SYMBOLIZATION]->(m)
CREATE (Amets)-[:NAME_RELATION]->(m)

CREATE (Aritz)-[:INTENSIVE]->(m)
CREATE (Aritz)-[:SYMBOLIZATION]->(m)
CREATE (Aritz)-[:NAME_RELATION]->(m)

CREATE (Arkaitz)-[:INTENSIVE]->(m)
CREATE (Arkaitz)-[:SYMBOLIZATION]->(m)
CREATE (Arkaitz)-[:NAME_RELATION]->(m)

CREATE (Eneko)-[:INTENSIVE]->(m)
CREATE (Eneko)-[:SYMBOLIZATION]->(m)
CREATE (Eneko)-[:NAME_RELATION]->(m)

CREATE (Fernando)-[:INTENSIVE]->(m)
CREATE (Fernando)-[:SYMBOLIZATION]->(m)
CREATE (Fernando)-[:NAME_RELATION]->(m)

CREATE (Ibai)-[:INTENSIVE]->(m)
CREATE (Ibai)-[:SYMBOLIZATION]->(m)
CREATE (Ibai)-[:NAME_RELATION]->(m)

CREATE (Iker)-[:INTENSIVE]->(m)
CREATE (Iker)-[:SYMBOLIZATION]->(m)
CREATE (Iker)-[:NAME_RELATION]->(m)

CREATE (Iñigo)-[:INTENSIVE]->(m)
CREATE (Iñigo)-[:SYMBOLIZATION]->(m)
CREATE (Iñigo)-[:NAME_RELATION]->(m)

CREATE (Ixak)-[:INTENSIVE]->(m)
CREATE (Ixak)-[:SYMBOLIZATION]->(m)
CREATE (Ixak)-[:NAME_RELATION]->(m)

CREATE (Jagoba)-[:INTENSIVE]->(m)
CREATE (Jagoba)-[:SYMBOLIZATION]->(m)
CREATE (Jagoba)-[:NAME_RELATION]->(m)

CREATE (Jon)-[:INTENSIVE]->(m)
CREATE (Jon)-[:SYMBOLIZATION]->(m)
CREATE (Jon)-[:NAME_RELATION]->(m)

CREATE (Joseba)-[:INTENSIVE]->(m)
CREATE (Joseba)-[:SYMBOLIZATION]->(m)
CREATE (Joseba)-[:NAME_RELATION]->(m)

CREATE (Josu)-[:INTENSIVE]->(m)
CREATE (Josu)-[:SYMBOLIZATION]->(m)
CREATE (Josu)-[:NAME_RELATION]->(m)

CREATE (Karra)-[:INTENSIVE]->(m)
CREATE (Karra)-[:SYMBOLIZATION]->(m)
CREATE (Karra)-[:NAME_RELATION]->(m)

CREATE (Kemen)-[:INTENSIVE]->(m)
CREATE (Kemen)-[:SYMBOLIZATION]->(m)
CREATE (Kemen)-[:NAME_RELATION]->(m)

CREATE (Oihan)-[:INTENSIVE]->(m)
CREATE (Oihan)-[:SYMBOLIZATION]->(m)
CREATE (Oihan)-[:NAME_RELATION]->(m)

CREATE (Oiher)-[:INTENSIVE]->(m)
CREATE (Oiher)-[:SYMBOLIZATION]->(m)
CREATE (Oiher)-[:NAME_RELATION]->(m)


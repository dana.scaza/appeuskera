MATCH (i:MusicalInstrument), (l:Language), (c:Clothes), (f:Food), (m:Furniture), (p:Publication), (wa:WrittenArtwork),  (wd:WrittenDocument), (s:Subject), (v:Vehicle), (dv:DrivableObject),  (listen:ListenableObject), (sing:SingableObject)
CREATE (alboka1:Common:Noun {level:"A1",root:"alboka"})
CREATE (bateria2:Common:Noun {level:"A1",root:"bateria"})
CREATE (baxu3:Common:Noun {level:"A1",root:"baxu"})
CREATE (biolin4:Common:Noun {level:"A1",root:"biolin"})
CREATE (danbor5:Common:Noun {level:"A1",root:"danbor"})
CREATE (dultzaina6:Common:Noun {level:"A1",root:"dultzaina"})
CREATE (eskusoinu7:Common:Noun {level:"A1",root:"eskusoinu"})
CREATE (harpa8:Common:Noun {level:"A1",root:"harpa"})
CREATE (gitarra9:Common:Noun {level:"A1",root:"gitarra"})
CREATE (pandereta10:Common:Noun {level:"A1",root:"pandereta"})
CREATE (piano11:Common:Noun {level:"A1",root:"piano"})
CREATE (saxofoi12:Common:Noun {level:"A1",root:"saxofoi"})
CREATE (trikitrixa13:Common:Noun {level:"A1",root:"trikitrixa"})
CREATE (turuta14:Common:Noun {level:"A1",root:"turuta"})
CREATE (txalaparta15:Common:Noun {level:"A1",root:"txalaparta"})
CREATE (aleman1:Mass:Noun {level:"A1",root:"aleman"})
CREATE (arabiera2:Mass:Noun {level:"A1",root:"arabiera"})
CREATE (errusiera3:Mass:Noun {level:"A1",root:"errusiera"})
CREATE (euskera4:Mass:Noun {level:"A1",root:"euskera"})
CREATE (frantses5:Mass:Noun {level:"A1",root:"frantses"})
CREATE (gaztelania6:Mass:Noun {level:"A1",root:"gaztelania"})
CREATE (ingeles7:Mass:Noun {level:"A1",root:"ingeles"})
CREATE (japoniera8:Mass:Noun {level:"A1",root:"japoniera"})
CREATE (koreera9:Mass:Noun {level:"A1",root:"koreera"})
CREATE (txinera10:Mass:Noun {level:"A1",root:"txinera"})
CREATE (alkandora1:Common:Noun {level:"A1",root:"alkandora"})
CREATE (beroki2:Common:Noun {level:"A1",root:"beroki"})
CREATE (gona3:Common:Noun {level:"A1",root:"gona"})
CREATE (jaka4:Common:Noun {level:"A1",root:"jaka"})
CREATE (jertse5:Common:Noun {level:"A1",root:"jertse"})
CREATE (lepo6:Common:Noun {level:"A1",root:"lepo-berogarri"})
CREATE (niki7:Common:Noun {level:"A1",root:"niki"})
CREATE (eskularru1:Common:Noun {defaultNumber:"Plural",level:"A1",root:"eskularru"})
CREATE (galtza2:Common:Noun {defaultNumber:"Plural",level:"A1",root:"galtza"})
CREATE (praka3:Common:Noun {defaultNumber:"Plural",level:"A1",root:"praka"})
CREATE (zapata4:Common:Noun {defaultNumber:"Plural",level:"A1",root:"zapata"})
CREATE (betaurreko5:Common:Noun {defaultNumber:"Plural",level:"A1",root:"betaurreko"})
CREATE (bakailao1:Mass:Noun {level:"A1",root:"bakailao"})
CREATE (bisigu2:Mass:Noun {level:"A1",root:"bisigu"})
CREATE (gazta3:Mass:Noun {level:"A1",root:"gazta"})
CREATE (jogurt4:Mass:Noun {level:"A1",root:"jogurt"})
CREATE (ogi5:Mass:Noun {level:"A1",root:"ogi"})
CREATE (haragi6:Mass:Noun {level:"A1",root:"haragi"})
CREATE (haragi7:Mass:Noun {level:"A1",root:"haragi gisatu"})
CREATE (hirugihar8:Mass:Noun {level:"A1",root:"hirugihar"})
CREATE (legatz9:Mass:Noun {level:"A1",root:"legatz"})
CREATE (oilasko10:Mass:Noun {level:"A1",root:"oilasko"})
CREATE (txerri11:Mass:Noun {level:"A1",root:"txerri"})
CREATE (txokolate12:Mass:Noun {level:"A1",root:"txokolate"})
CREATE (anana1:Common:Noun {defaultNumber:"Plural",level:"A1",root:"anana"})
CREATE (antxoa2:Common:Noun {defaultNumber:"Plural",level:"A1",root:"antxoa"})
CREATE (babarrun3:Common:Noun {defaultNumber:"Plural",level:"A1",root:"babarrun"})
CREATE (gaileta4:Common:Noun {defaultNumber:"Plural",level:"A1",root:"gaileta"})
CREATE (gaztaina5:Common:Noun {defaultNumber:"Plural",level:"A1",root:"gaztaina"})
CREATE (gerezi6:Common:Noun {defaultNumber:"Plural",level:"A1",root:"gerezi"})
CREATE (gozoki7:Common:Noun {defaultNumber:"Plural",level:"A1",root:"gozoki"})
CREATE (intxaur8:Common:Noun {defaultNumber:"Plural",level:"A1",root:"intxaur"})
CREATE (kiwi9:Common:Noun {defaultNumber:"Plural",level:"A1",root:"kiwi"})
CREATE (laranja10:Common:Noun {defaultNumber:"Plural",level:"A1",root:"laranja"})
CREATE (mahats11:Common:Noun {defaultNumber:"Plural",level:"A1",root:"mahats"})
CREATE (marrubi12:Common:Noun {defaultNumber:"Plural",level:"A1",root:"marrubi"})
CREATE (mertxika13:Common:Noun {defaultNumber:"Plural",level:"A1",root:"mertxika"})
CREATE (piku14:Common:Noun {defaultNumber:"Plural",level:"A1",root:"piku"})
CREATE (sardina15:Common:Noun {defaultNumber:"Plural",level:"A1",root:"sardina"})
CREATE (udare16:Common:Noun {defaultNumber:"Plural",level:"A1",root:"udare"})
CREATE (aldizkari1:Common:Noun {level:"A1",root:"aldizkari"})
CREATE (egunkari2:Common:Noun {level:"A1",root:"egunkari"})
CREATE (liburu3:Common:Noun {level:"A1",root:"liburu"})
CREATE (abentura1:Common:Noun {level:"A1",root:"abentura-nobela"})
CREATE (ipuin2:Common:Noun {level:"A1",root:"ipuin"})
CREATE (nobela3:Common:Noun {level:"A1",root:"nobela"})
CREATE (nobela4:Common:Noun {level:"A1",root:"nobela beltz"})
CREATE (nobela5:Common:Noun {level:"A1",root:"nobela historiko"})
CREATE (olerki6:Common:Noun {level:"A1",root:"olerki"})
CREATE (poema7:Common:Noun {level:"A1",root:"poema"})
CREATE (eskutitz1:Common:Noun {level:"A1",root:"eskutitz"})
CREATE (gutun2:Common:Noun {level:"A1",root:"gutun"})
CREATE (mezu3:Common:Noun {level:"A1",root:"mezu"})
CREATE (txosten4:Common:Noun {level:"A1",root:"txosten"})
CREATE (matematika1:Mass:Noun {level:"A1",root:"matematika"})
CREATE (fisika2:Mass:Noun {level:"A1",root:"fisika"})
CREATE (historia3:Mass:Noun {level:"A1",root:"historia"})
CREATE (geografia4:Mass:Noun {level:"A1",root:"geografia"})
CREATE (bizikleta1:Common:Noun {level:"A1",root:"bizikleta"})
CREATE (hegazkin2:Common:Noun {level:"A1",root:"hegazkin"})
CREATE (helikoptero3:Common:Noun {level:"A1",root:"helikoptero"})
CREATE (patinete4:Common:Noun {level:"A1",root:"patinete"})
CREATE (tren5:Common:Noun {level:"A1",root:"tren"})
CREATE (txirringa6:Common:Noun {level:"A1",root:"txirringa"})
CREATE (autobus1:Common:Noun {level:"A1",root:"autobus"})
CREATE (automobil2:Common:Noun {level:"A1",root:"automobil"})
CREATE (kamioi3:Common:Noun {level:"A1",root:"kamioi"})
CREATE (tratore4:Common:Noun {level:"A1",root:"tratore"})
CREATE (kanta1:Common:Noun {level:"A1",root:"kanta"})
CREATE (abesti2:Common:Noun {level:"A1",root:"abesti"})
CREATE (Gabon3:Common:Noun {level:"A1",root:"Gabon-kanta"})
CREATE (lo4:Common:Noun {level:"A1",root:"lo-kanta"})
CREATE (musika1:Mass:Noun {level:"A1",root:"musika"})
CREATE (irrati2:Mass:Noun {level:"A1",root:"irrati"})
CREATE (alboka1)-[:INTENSIVE]->(i)
CREATE (alboka1)-[:IDENTITY]->(i)

CREATE (bateria2)-[:INTENSIVE]->(i)
CREATE (bateria2)-[:IDENTITY]->(i)

CREATE (baxu3)-[:INTENSIVE]->(i)
CREATE (baxu3)-[:IDENTITY]->(i)

CREATE (biolin4)-[:INTENSIVE]->(i)
CREATE (biolin4)-[:IDENTITY]->(i)

CREATE (danbor5)-[:INTENSIVE]->(i)
CREATE (danbor5)-[:IDENTITY]->(i)

CREATE (dultzaina6)-[:INTENSIVE]->(i)
CREATE (dultzaina6)-[:IDENTITY]->(i)

CREATE (eskusoinu7)-[:INTENSIVE]->(i)
CREATE (eskusoinu7)-[:IDENTITY]->(i)

CREATE (harpa8)-[:INTENSIVE]->(i)
CREATE (harpa8)-[:IDENTITY]->(i)

CREATE (gitarra9)-[:INTENSIVE]->(i)
CREATE (gitarra9)-[:IDENTITY]->(i)

CREATE (pandereta10)-[:INTENSIVE]->(i)
CREATE (pandereta10)-[:IDENTITY]->(i)

CREATE (piano11)-[:INTENSIVE]->(i)
CREATE (piano11)-[:IDENTITY]->(i)

CREATE (saxofoi12)-[:INTENSIVE]->(i)
CREATE (saxofoi12)-[:IDENTITY]->(i)

CREATE (trikitrixa13)-[:INTENSIVE]->(i)
CREATE (trikitrixa13)-[:IDENTITY]->(i)

CREATE (turuta14)-[:INTENSIVE]->(i)
CREATE (turuta14)-[:IDENTITY]->(i)

CREATE (txalaparta15)-[:INTENSIVE]->(i)
CREATE (txalaparta15)-[:IDENTITY]->(i)

CREATE (aleman1)-[:INTENSIVE]->(l)
CREATE (aleman1)-[:IDENTITY]->(l)

CREATE (arabiera2)-[:INTENSIVE]->(l)
CREATE (arabiera2)-[:IDENTITY]->(l)

CREATE (errusiera3)-[:INTENSIVE]->(l)
CREATE (errusiera3)-[:IDENTITY]->(l)

CREATE (euskera4)-[:INTENSIVE]->(l)
CREATE (euskera4)-[:IDENTITY]->(l)

CREATE (frantses5)-[:INTENSIVE]->(l)
CREATE (frantses5)-[:IDENTITY]->(l)

CREATE (gaztelania6)-[:INTENSIVE]->(l)
CREATE (gaztelania6)-[:IDENTITY]->(l)

CREATE (ingeles7)-[:INTENSIVE]->(l)
CREATE (ingeles7)-[:IDENTITY]->(l)

CREATE (japoniera8)-[:INTENSIVE]->(l)
CREATE (japoniera8)-[:IDENTITY]->(l)

CREATE (koreera9)-[:INTENSIVE]->(l)
CREATE (koreera9)-[:IDENTITY]->(l)

CREATE (txinera10)-[:INTENSIVE]->(l)
CREATE (txinera10)-[:IDENTITY]->(l)

CREATE (alkandora1)-[:INTENSIVE]->(c)
CREATE (alkandora1)-[:IDENTITY]->(c)

CREATE (beroki2)-[:INTENSIVE]->(c)
CREATE (beroki2)-[:IDENTITY]->(c)

CREATE (gona3)-[:INTENSIVE]->(c)
CREATE (gona3)-[:IDENTITY]->(c)

CREATE (jaka4)-[:INTENSIVE]->(c)
CREATE (jaka4)-[:IDENTITY]->(c)

CREATE (jertse5)-[:INTENSIVE]->(c)
CREATE (jertse5)-[:IDENTITY]->(c)

CREATE (lepo6)-[:INTENSIVE]->(c)
CREATE (lepo6)-[:IDENTITY]->(c)

CREATE (niki7)-[:INTENSIVE]->(c)
CREATE (niki7)-[:IDENTITY]->(c)

CREATE (eskularru1)-[:INTENSIVE]->(c)
CREATE (eskularru1)-[:IDENTITY]->(c)

CREATE (galtza2)-[:INTENSIVE]->(c)
CREATE (galtza2)-[:IDENTITY]->(c)

CREATE (praka3)-[:INTENSIVE]->(c)
CREATE (praka3)-[:IDENTITY]->(c)

CREATE (zapata4)-[:INTENSIVE]->(c)
CREATE (zapata4)-[:IDENTITY]->(c)

CREATE (betaurreko5)-[:INTENSIVE]->(c)
CREATE (betaurreko5)-[:IDENTITY]->(c)

CREATE (bakailao1)-[:INTENSIVE]->(f)
CREATE (bakailao1)-[:IDENTITY]->(f)

CREATE (bisigu2)-[:INTENSIVE]->(f)
CREATE (bisigu2)-[:IDENTITY]->(f)

CREATE (gazta3)-[:INTENSIVE]->(f)
CREATE (gazta3)-[:IDENTITY]->(f)

CREATE (jogurt4)-[:INTENSIVE]->(f)
CREATE (jogurt4)-[:IDENTITY]->(f)

CREATE (ogi5)-[:INTENSIVE]->(f)
CREATE (ogi5)-[:IDENTITY]->(f)

CREATE (haragi6)-[:INTENSIVE]->(f)
CREATE (haragi6)-[:IDENTITY]->(f)

CREATE (haragi7)-[:INTENSIVE]->(f)
CREATE (haragi7)-[:IDENTITY]->(f)

CREATE (hirugihar8)-[:INTENSIVE]->(f)
CREATE (hirugihar8)-[:IDENTITY]->(f)

CREATE (legatz9)-[:INTENSIVE]->(f)
CREATE (legatz9)-[:IDENTITY]->(f)

CREATE (oilasko10)-[:INTENSIVE]->(f)
CREATE (oilasko10)-[:IDENTITY]->(f)

CREATE (txerri11)-[:INTENSIVE]->(f)
CREATE (txerri11)-[:IDENTITY]->(f)

CREATE (txokolate12)-[:INTENSIVE]->(f)
CREATE (txokolate12)-[:IDENTITY]->(f)

CREATE (anana1)-[:INTENSIVE]->(f)
CREATE (anana1)-[:IDENTITY]->(f)

CREATE (antxoa2)-[:INTENSIVE]->(f)
CREATE (antxoa2)-[:IDENTITY]->(f)

CREATE (babarrun3)-[:INTENSIVE]->(f)
CREATE (babarrun3)-[:IDENTITY]->(f)

CREATE (gaileta4)-[:INTENSIVE]->(f)
CREATE (gaileta4)-[:IDENTITY]->(f)

CREATE (gaztaina5)-[:INTENSIVE]->(f)
CREATE (gaztaina5)-[:IDENTITY]->(f)

CREATE (gerezi6)-[:INTENSIVE]->(f)
CREATE (gerezi6)-[:IDENTITY]->(f)

CREATE (gozoki7)-[:INTENSIVE]->(f)
CREATE (gozoki7)-[:IDENTITY]->(f)

CREATE (intxaur8)-[:INTENSIVE]->(f)
CREATE (intxaur8)-[:IDENTITY]->(f)

CREATE (kiwi9)-[:INTENSIVE]->(f)
CREATE (kiwi9)-[:IDENTITY]->(f)

CREATE (laranja10)-[:INTENSIVE]->(f)
CREATE (laranja10)-[:IDENTITY]->(f)

CREATE (mahats11)-[:INTENSIVE]->(f)
CREATE (mahats11)-[:IDENTITY]->(f)

CREATE (marrubi12)-[:INTENSIVE]->(f)
CREATE (marrubi12)-[:IDENTITY]->(f)

CREATE (mertxika13)-[:INTENSIVE]->(f)
CREATE (mertxika13)-[:IDENTITY]->(f)

CREATE (piku14)-[:INTENSIVE]->(f)
CREATE (piku14)-[:IDENTITY]->(f)

CREATE (sardina15)-[:INTENSIVE]->(f)
CREATE (sardina15)-[:IDENTITY]->(f)

CREATE (udare16)-[:INTENSIVE]->(f)
CREATE (udare16)-[:IDENTITY]->(f)

CREATE (aldizkari1)-[:INTENSIVE]->(p)
CREATE (aldizkari1)-[:IDENTITY]->(p)

CREATE (egunkari2)-[:INTENSIVE]->(p)
CREATE (egunkari2)-[:IDENTITY]->(p)

CREATE (liburu3)-[:INTENSIVE]->(p)
CREATE (liburu3)-[:IDENTITY]->(p)

CREATE (eskutitz1)-[:INTENSIVE]->(wd)
CREATE (eskutitz1)-[:IDENTITY]->(wd)

CREATE (gutun2)-[:INTENSIVE]->(wd)
CREATE (gutun2)-[:IDENTITY]->(wd)

CREATE (mezu3)-[:INTENSIVE]->(wd)
CREATE (mezu3)-[:IDENTITY]->(wd)

CREATE (txosten4)-[:INTENSIVE]->(wd)
CREATE (txosten4)-[:IDENTITY]->(wd)

CREATE (abentura1)-[:INTENSIVE]->(wa)
CREATE (abentura1)-[:IDENTITY]->(wa)

CREATE (ipuin2)-[:INTENSIVE]->(wa)
CREATE (ipuin2)-[:IDENTITY]->(wa)

CREATE (nobela3)-[:INTENSIVE]->(wa)
CREATE (nobela3)-[:IDENTITY]->(wa)

CREATE (nobela4)-[:INTENSIVE]->(wa)
CREATE (nobela4)-[:IDENTITY]->(wa)

CREATE (nobela5)-[:INTENSIVE]->(wa)
CREATE (nobela5)-[:IDENTITY]->(wa)

CREATE (olerki6)-[:INTENSIVE]->(wa)
CREATE (olerki6)-[:IDENTITY]->(wa)

CREATE (poema7)-[:INTENSIVE]->(wa)
CREATE (poema7)-[:IDENTITY]->(wa)

CREATE (bizikleta1)-[:INTENSIVE]->(v)
CREATE (bizikleta1)-[:IDENTITY]->(v)

CREATE (hegazkin2)-[:INTENSIVE]->(v)
CREATE (hegazkin2)-[:IDENTITY]->(v)

CREATE (helikoptero3)-[:INTENSIVE]->(v)
CREATE (helikoptero3)-[:IDENTITY]->(v)

CREATE (patinete4)-[:INTENSIVE]->(v)
CREATE (patinete4)-[:IDENTITY]->(v)

CREATE (tren5)-[:INTENSIVE]->(v)
CREATE (tren5)-[:IDENTITY]->(v)

CREATE (txirringa6)-[:INTENSIVE]->(v)
CREATE (txirringa6)-[:IDENTITY]->(v)

CREATE (autobus1)-[:INTENSIVE]->(dv)
CREATE (autobus1)-[:IDENTITY]->(dv)

CREATE (automobil2)-[:INTENSIVE]->(dv)
CREATE (automobil2)-[:IDENTITY]->(dv)

CREATE (kamioi3)-[:INTENSIVE]->(dv)
CREATE (kamioi3)-[:IDENTITY]->(dv)

CREATE (tratore4)-[:INTENSIVE]->(dv)
CREATE (tratore4)-[:IDENTITY]->(dv)

CREATE (musika1)-[:INTENSIVE]->(listen)
CREATE (musika1)-[:IDENTITY]->(listen)

CREATE (irrati2)-[:INTENSIVE]->(listen)
CREATE (irrati2)-[:IDENTITY]->(listen)

CREATE (kanta1)-[:INTENSIVE]->(sing)
CREATE (kanta1)-[:IDENTITY]->(sing)

CREATE (abesti2)-[:INTENSIVE]->(sing)
CREATE (abesti2)-[:IDENTITY]->(sing)

CREATE (Gabon3)-[:INTENSIVE]->(sing)
CREATE (Gabon3)-[:IDENTITY]->(sing)

CREATE (lo4)-[:INTENSIVE]->(sing)
CREATE (lo4)-[:IDENTITY]->(sing)

CREATE (matematika1)-[:INTENSIVE]->(s)
CREATE (matematika1)-[:IDENTITY]->(s)

CREATE (fisika2)-[:INTENSIVE]->(s)
CREATE (fisika2)-[:IDENTITY]->(s)

CREATE (historia3)-[:INTENSIVE]->(s)
CREATE (historia3)-[:IDENTITY]->(s)

CREATE (geografia4)-[:INTENSIVE]->(s)
CREATE (geografia4)-[:IDENTITY]->(s)


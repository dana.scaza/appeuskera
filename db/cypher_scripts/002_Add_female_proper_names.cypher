MATCH (f:Female)-[:IDENTITY]->(:Person)
CREATE (Aroa:Anthroponym:Noun {level:"A1",root:"Aroa"})
CREATE (Ainhoa:Anthroponym:Noun {level:"A1",root:"Ainhoa"})
CREATE (Ainara:Anthroponym:Noun {level:"A1",root:"Ainara"})
CREATE (Ana:Anthroponym:Noun {level:"A1",root:"Ana"})
CREATE (Amaia:Anthroponym:Noun {level:"A1",root:"Amaia"})
CREATE (Barbara:Anthroponym:Noun {level:"A1",root:"Barbara"})
CREATE (Begoña:Anthroponym:Noun {level:"A1",root:"Begoña"})
CREATE (Iraia:Anthroponym:Noun {level:"A1",root:"Iraia"})
CREATE (Itsaso:Anthroponym:Noun {level:"A1",root:"Itsaso"})
CREATE (Jaione:Anthroponym:Noun {level:"A1",root:"Jaione"})
CREATE (June:Anthroponym:Noun {level:"A1",root:"June"})
CREATE (Lourdes:Anthroponym:Noun {level:"A1",root:"Lourdes"})
CREATE (Lur:Anthroponym:Noun {level:"A1",root:"Lur"})
CREATE (Maider:Anthroponym:Noun {level:"A1",root:"Maider"})
CREATE (Maite:Anthroponym:Noun {level:"A1",root:"Maite"})
CREATE (Mar:Anthroponym:Noun {level:"A1",root:"Mar"})
CREATE (Maren:Anthroponym:Noun {level:"A1",root:"Maren"})
CREATE (Naroa:Anthroponym:Noun {level:"A1",root:"Naroa"})
CREATE (Nieves:Anthroponym:Noun {level:"A1",root:"Nieves"})
CREATE (Oneka:Anthroponym:Noun {level:"A1",root:"Oneka"})
CREATE (Rosa:Anthroponym:Noun {level:"A1",root:"Rosa"})
CREATE (Uxue:Anthroponym:Noun {level:"A1",root:"Uxue"})
CREATE (Zoe:Anthroponym:Noun {level:"A1",root:"Zoe"})
CREATE (Aroa)-[:INTENSIVE]->(f)
CREATE (Aroa)-[:SYMBOLIZATION]->(f)
CREATE (Aroa)-[:NAME_RELATION]->(f)

CREATE (Ainhoa)-[:INTENSIVE]->(f)
CREATE (Ainhoa)-[:SYMBOLIZATION]->(f)
CREATE (Ainhoa)-[:NAME_RELATION]->(f)

CREATE (Ainara)-[:INTENSIVE]->(f)
CREATE (Ainara)-[:SYMBOLIZATION]->(f)
CREATE (Ainara)-[:NAME_RELATION]->(f)

CREATE (Ana)-[:INTENSIVE]->(f)
CREATE (Ana)-[:SYMBOLIZATION]->(f)
CREATE (Ana)-[:NAME_RELATION]->(f)

CREATE (Amaia)-[:INTENSIVE]->(f)
CREATE (Amaia)-[:SYMBOLIZATION]->(f)
CREATE (Amaia)-[:NAME_RELATION]->(f)

CREATE (Barbara)-[:INTENSIVE]->(f)
CREATE (Barbara)-[:SYMBOLIZATION]->(f)
CREATE (Barbara)-[:NAME_RELATION]->(f)

CREATE (Begoña)-[:INTENSIVE]->(f)
CREATE (Begoña)-[:SYMBOLIZATION]->(f)
CREATE (Begoña)-[:NAME_RELATION]->(f)

CREATE (Iraia)-[:INTENSIVE]->(f)
CREATE (Iraia)-[:SYMBOLIZATION]->(f)
CREATE (Iraia)-[:NAME_RELATION]->(f)

CREATE (Itsaso)-[:INTENSIVE]->(f)
CREATE (Itsaso)-[:SYMBOLIZATION]->(f)
CREATE (Itsaso)-[:NAME_RELATION]->(f)

CREATE (Jaione)-[:INTENSIVE]->(f)
CREATE (Jaione)-[:SYMBOLIZATION]->(f)
CREATE (Jaione)-[:NAME_RELATION]->(f)

CREATE (June)-[:INTENSIVE]->(f)
CREATE (June)-[:SYMBOLIZATION]->(f)
CREATE (June)-[:NAME_RELATION]->(f)

CREATE (Lourdes)-[:INTENSIVE]->(f)
CREATE (Lourdes)-[:SYMBOLIZATION]->(f)
CREATE (Lourdes)-[:NAME_RELATION]->(f)

CREATE (Lur)-[:INTENSIVE]->(f)
CREATE (Lur)-[:SYMBOLIZATION]->(f)
CREATE (Lur)-[:NAME_RELATION]->(f)

CREATE (Maider)-[:INTENSIVE]->(f)
CREATE (Maider)-[:SYMBOLIZATION]->(f)
CREATE (Maider)-[:NAME_RELATION]->(f)

CREATE (Maite)-[:INTENSIVE]->(f)
CREATE (Maite)-[:SYMBOLIZATION]->(f)
CREATE (Maite)-[:NAME_RELATION]->(f)

CREATE (Mar)-[:INTENSIVE]->(f)
CREATE (Mar)-[:SYMBOLIZATION]->(f)
CREATE (Mar)-[:NAME_RELATION]->(f)

CREATE (Maren)-[:INTENSIVE]->(f)
CREATE (Maren)-[:SYMBOLIZATION]->(f)
CREATE (Maren)-[:NAME_RELATION]->(f)

CREATE (Naroa)-[:INTENSIVE]->(f)
CREATE (Naroa)-[:SYMBOLIZATION]->(f)
CREATE (Naroa)-[:NAME_RELATION]->(f)

CREATE (Nieves)-[:INTENSIVE]->(f)
CREATE (Nieves)-[:SYMBOLIZATION]->(f)
CREATE (Nieves)-[:NAME_RELATION]->(f)

CREATE (Oneka)-[:INTENSIVE]->(f)
CREATE (Oneka)-[:SYMBOLIZATION]->(f)
CREATE (Oneka)-[:NAME_RELATION]->(f)

CREATE (Rosa)-[:INTENSIVE]->(f)
CREATE (Rosa)-[:SYMBOLIZATION]->(f)
CREATE (Rosa)-[:NAME_RELATION]->(f)

CREATE (Uxue)-[:INTENSIVE]->(f)
CREATE (Uxue)-[:SYMBOLIZATION]->(f)
CREATE (Uxue)-[:NAME_RELATION]->(f)

CREATE (Zoe)-[:INTENSIVE]->(f)
CREATE (Zoe)-[:SYMBOLIZATION]->(f)
CREATE (Zoe)-[:NAME_RELATION]->(f)


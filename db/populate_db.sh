cat cypher_scripts/001_Categories.cypher | cypher-shell -u neo4j -p euskera
cat cypher_scripts/002_Add_female_proper_names.cypher | cypher-shell -u neo4j -p euskera
cat cypher_scripts/003_Add_male_proper_names.cypher | cypher-shell -u neo4j -p euskera
cat cypher_scripts/004_Add_jobs.cypher | cypher-shell -u neo4j -p euskera
cat cypher_scripts/005_Add_adjectives.cypher | cypher-shell -u neo4j -p euskera 
cat cypher_scripts/006_Add_general_vocabulary.cypher | cypher-shell -u neo4j -p euskera
cat cypher_scripts/007_Add_nor_nork_verbs.cypher | cypher-shell -u neo4j -p euskera
cat cypher_scripts/008_Add_person_ownership.cypher | cypher-shell -u neo4j -p euskera


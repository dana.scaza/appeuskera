import qualified Data.Map as M
import System.IO
import Utils

main :: IO ()
main = do
  -- read files
  female_names <- readFile "../word_lists/female_names.txt"
  male_names <- readFile "../word_lists/male_names.txt"
  let female_nodes = map anthroponymNode (lines female_names)
  let male_nodes = map anthroponymNode (lines male_names)
  writeFile "../cypher_scripts/002_Add_female_proper_names.cypher" $
    "MATCH (f:Female)-[:IDENTITY]->(:Person)\n"
      ++ unlines (createNode <$> female_nodes)
      ++ unlines (femaleNAME_RELATIONship <$> female_nodes)
  writeFile "../cypher_scripts/003_Add_male_proper_names.cypher" $
    "MATCH (m:Male)-[:IDENTITY]->(:Person)\n"
      ++ unlines (createNode <$> male_nodes)
      ++ unlines (maleNAME_RELATIONship <$> male_nodes)

femaleNAME_RELATIONship srcNode =
  createRelationship
    "NAME_RELATION"
    srcNode
    dummyFemaleNode

maleNAME_RELATIONship srcNode =
  createRelationship
    "NAME_RELATION"
    srcNode
    dummyMaleNode

dummyFemaleNode = Node "f" ""

dummyMaleNode = Node "m" ""

anthroponymNode name =
  Node name $
    basicNode
      name
      "Anthroponym:Noun"
      (rootA1Properties name)

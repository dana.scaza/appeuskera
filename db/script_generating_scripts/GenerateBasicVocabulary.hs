import qualified Data.Map as M
import System.IO
import Utils

main :: IO ()
main = do
  instruments <- getNodes commonNounNode "musical_instruments"
  languages <- getNodes massNounNode "languages"
  clothes <- getNodes commonNounNode "clothes"
  plural_clothes <- getNodes pluralNounNode "clothes_plural"
  mass_food <- getNodes massNounNode "edible_things_mass"
  plural_food <- getNodes pluralNounNode "edible_things_plural"
  furniture <- getNodes commonNounNode "furniture"
  publications <- getNodes commonNounNode "readable_things"
  written_artwork <- getNodes commonNounNode "written_artwork"
  written_docs <- getNodes commonNounNode "written_documents"
  vehicles <- getNodes commonNounNode "vehicles"
  drivable <- getNodes commonNounNode "drivable"
  listenable <- getNodes massNounNode "listenable_things"
  singable <- getNodes commonNounNode "singable_things"
  subjects <- getNodes massNounNode "study_subjects"
  writeFile "../cypher_scripts/006_Add_general_vocabulary.cypher" $
    "MATCH (i:MusicalInstrument), (l:Language), (c:Clothes), "
      ++ "(f:Food), (m:Furniture), (p:Publication), (wa:WrittenArtwork), "
      ++ " (wd:WrittenDocument), (s:Subject), (v:Vehicle), (dv:DrivableObject), "
      ++ " (listen:ListenableObject), (sing:SingableObject)\n"
      ++ unlines
        ( createNode
            <$> concat
              [ instruments,
                languages,
                clothes,
                plural_clothes,
                mass_food,
                plural_food,
                publications,
                written_artwork,
                written_docs,
                subjects,
                vehicles,
                drivable,
                singable,
                listenable
              ]
        )
      ++ unlines (identityRelationship (dummyNode "i") <$> instruments)
      ++ unlines (identityRelationship (dummyNode "l") <$> languages)
      ++ unlines (identityRelationship (dummyNode "c") <$> clothes ++ plural_clothes)
      ++ unlines (identityRelationship (dummyNode "f") <$> mass_food ++ plural_food)
      ++ unlines (identityRelationship (dummyNode "p") <$> publications)
      ++ unlines (identityRelationship (dummyNode "wd") <$> written_docs)
      ++ unlines (identityRelationship (dummyNode "wa") <$> written_artwork)
      ++ unlines (identityRelationship (dummyNode "v") <$> vehicles)
      ++ unlines (identityRelationship (dummyNode "dv") <$> drivable)
      ++ unlines (identityRelationship (dummyNode "listen") <$> listenable)
      ++ unlines (identityRelationship (dummyNode "sing") <$> singable)
      ++ unlines (identityRelationship (dummyNode "s") <$> subjects)

getNodes nodeTransformF filename = do
  file <- readFile ("../word_lists/" ++ filename ++ ".txt")
  return $ zipWith nodeTransformF [1 ..] (lines file)

nounNode nounClass num name =
  Node identifier $
    basicNode
      identifier
      (nounClass ++ ":Noun")
      (rootA1Properties name)
  where
    identifier = preprocessId name ++ show num

commonNounNode = nounNode "Common"

massNounNode = nounNode "Mass"

pluralNounNode num name =
  Node identifier $
    basicNode
      identifier
      "Common:Noun"
      (pluralProperty (rootA1Properties name))
  where
    identifier = preprocessId name ++ show num

dummyNode ident = Node ident ""

identityRelationship tgtNode srcNode =
  createRelationship
    "IDENTITY"
    srcNode
    tgtNode

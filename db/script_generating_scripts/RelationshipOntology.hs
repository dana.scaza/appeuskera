module RelationshipOntology where

import Data.Tree

-- | The ontology of relationship types in the database
ontology :: Tree String
ontology =
  Node
    "INTENSIVE"
    [ ascription,
      identity,
      symbolization
    ]

-- | Path to the given category, including the category itself
pathInOntology :: Category -> [Category]
pathInOntology = pathToNode ontology

--------------------------------------------------------------------------------

type Category = String

ascription =
  Node
    "ASCRIPTION"
    [ Node "CLASS_ASCRIPTION" [],
      propertyASCRIPTION
    ]

propertyASCRIPTION =
  Node
    "PROPERTY_ASCRIPTION"
    [ Node "PROVENANCE_PROPERTY_ASCRIPTION" [],
      Node "MATERIAL_PROPERTY_ASCRIPTION" []
    ]

symbolization = Node "SYMBOLIZATION" [Node "NAME_RELATION" []]

identity = Node "IDENTITY" []

-- we could use trie if this didn't perform well

pathToNode :: (Eq a) => Tree a -> a -> [a]
pathToNode tree node = case pathToNode' node tree of
  (path : _) -> path
  _ -> error "Value not found"

pathToNode' :: (Eq a) => a -> Tree a -> [[a]]
pathToNode' node (Node cat subcat)
  | node == cat = [[node]]
  | otherwise = concat [map (cat :) (pathToNode' node s) | s <- subcat]

-- this works because map f [] = []

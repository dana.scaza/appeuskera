import qualified Data.Map as M
import System.IO
import Utils

main :: IO ()
main = do
  -- read files
  jobs_file <- readFile "../word_lists/jobs.txt"
  let jobs = map (uncurry jobNode) (zip [1 ..] (lines jobs_file))
  writeFile "../cypher_scripts/004_Add_jobs.cypher" $
    "MATCH (j:Occupation), (p:Person)\n"
      ++ unlines (createNode <$> jobs)
      ++ unlines (isAJobRelationship <$> jobs)
      ++ unlines (personHasAJobRelationship <$> jobs)

jobNode num name =
  Node identifier $
    basicNode
      identifier
      "Common:Noun"
      (rootA1Properties name)
  where
    identifier = (preprocessId name) ++ (show num)

dummyJobNode = Node "j" ""

dummyPersonNode = Node "p" ""

isAJobRelationship srcNode =
  createRelationship
    "IDENTITY"
    srcNode
    dummyJobNode

personHasAJobRelationship tgtNode =
  createRelationship
    "CLASS_ASCRIPTION"
    dummyPersonNode
    tgtNode

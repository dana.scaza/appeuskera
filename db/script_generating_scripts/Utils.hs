module Utils where

import Data.List
import qualified Data.Map as M
import RelationshipOntology

-- Types
--------------------------------------------------------------------------------
type RelationshipType = String -- camel case, beginning uppercase

type NodeLabel = String -- upper case, with underscores

type Property = String -- camel case, beginning with lowercase

type Cypher = String -- cypher code

-- Relationships
--------------------------------------------------------------------------------

createNode :: Node -> Cypher
createNode node = "CREATE " ++ (getNode node)

createRelationship :: RelationshipType -> Node -> Node -> Cypher
createRelationship relType start end =
  unlines $ map createStatement (pathInOntology relType)
  where
    createStatement rel = "CREATE " ++ (relationship rel start end)

relationshipType :: RelationshipType -> Cypher
relationshipType r = "[:" ++ r ++ "]"

nodeId :: Node -> Cypher
nodeId node = "(" ++ (getIdentifier node) ++ ")"

relationship :: RelationshipType -> Node -> Node -> Cypher
relationship relType start end =
  (nodeId start)
    ++ "-"
    ++ (relationshipType relType)
    ++ "->"
    ++ (nodeId end)

-- Nodes
--------------------------------------------------------------------------------
data Node = Node {getIdentifier :: String, getNode :: String}
  deriving (Eq, Show)

basicNode :: String -> NodeLabel -> M.Map Property String -> Cypher
basicNode identifier nodeLabel propertyMap =
  "("
    ++ identifier
    ++ ":"
    ++ nodeLabel
    ++ " {"
    ++ propertyList
    ++ "})"
  where
    propertyList =
      concat $
        intersperse "," $
          M.foldr (:) [] $
            M.mapWithKey (\k v -> k ++ ":" ++ v) propertyMap

--makeNodes :: [String] -> Int -> [Node]
--makeNodes [] _ _  = []
--makeNodes (x:xs) acc = (properNameNode identifier x):(makeNodes xs gender (acc+1))
--         where identifier = gender ++ (show acc)

addQuotes :: String -> String
addQuotes str = "\"" ++ str ++ "\""

emptyId :: String
emptyId = ""

preprocessId :: String -> String
preprocessId = takeWhile (\x -> (x /= '-') && (x /= ' '))

rootProperty :: String -> M.Map Property String
rootProperty p = M.insert "root" (addQuotes p) M.empty

rootA1Properties :: String -> M.Map Property String
rootA1Properties root = M.insert "level" (addQuotes "A1") $ rootProperty root

pluralProperty :: M.Map Property String -> M.Map Property String
pluralProperty = M.insert "defaultNumber" (addQuotes "Plural")

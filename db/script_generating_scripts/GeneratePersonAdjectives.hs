import qualified Data.Map as M
import System.IO
import Utils

main :: IO ()
main = do
  -- read files
  adjs1_file <- readFile "../word_lists/person_adjectives_character.txt"
  adjs2_file <- readFile "../word_lists/person_adjectives_physical.txt"
  let adjs1 = map (uncurry adjNode) (zip [1 ..] (lines adjs1_file))
  let adjs2 = map (uncurry adjNode) (zip [1 ..] (lines adjs2_file))
  writeFile "../cypher_scripts/005_Add_adjectives.cypher" $
    "MATCH (p:Person)\n"
      ++ unlines (createNode <$> adjs1)
      ++ unlines (createNode <$> adjs2)
      ++ unlines (personHasAQualityRelationship <$> adjs1)
      ++ unlines (personHasAPhysicalQualityRelationship <$> adjs2)

adjNode num name =
  Node identifier $
    basicNode
      identifier
      "Adjective"
      (rootA1Properties name)
  where
    identifier = preprocessId name ++ show num

dummyPersonNode = Node "p" ""

personHasAQualityRelationship =
  createRelationship
    "PROPERTY_ASCRIPTION"
    dummyPersonNode

personHasAPhysicalQualityRelationship =
  createRelationship
    "MATERIAL_PROPERTY_ASCRIPTION"
    dummyPersonNode
